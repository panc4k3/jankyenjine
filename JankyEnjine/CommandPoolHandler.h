#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "DeviceHandler.h"

class CommandPoolHandler
{
	public:
		VkResult createCommandPool(VkDevice& device, QueueFamilyIndices& indices);
		VkResult createBufferCommandPool(VkDevice& device, QueueFamilyIndices& indices);
		VkCommandPool& getCommandPool();
		VkCommandPool& getBufferCommandPool();
		void Destroy(VkDevice& device);

	private:
		VkCommandPool commandPool;
		VkCommandPool bufferCommandPool;
};

