#include "RoomMap.h"

RoomMap::RoomMap(std::map<uint32_t, Room> rooms)
{
	m_rooms = rooms;
}

RoomMap::~RoomMap() {

}

void RoomMap::addRoom(uint32_t roomId, Room room) {
	m_rooms.emplace(roomId, room);
}

Room RoomMap::getRoom(uint32_t roomId) {
	return m_rooms.find(roomId)->second;
}
