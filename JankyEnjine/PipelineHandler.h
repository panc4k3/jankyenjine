#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>

#include "Reader.h"
#include "StructsHandler.h"
#include "Settings.h"

class PipelineHandler
{
	public:
		VkResult createGraphicsPipeline(VkDevice& device, VkExtent2D& swapChainExtent, VkRenderPass& renderPass, std::vector<VkDescriptorSetLayout>& descriptorSetLayouts);
		VkPipeline& getGraphicsPipeline();
		VkPipelineLayout& getPipelineLayout();
		void Destroy(VkDevice& device);

	private:
		VkShaderModule vertShaderModule;
		VkShaderModule fragShaderModule;

		VkPipelineLayout pipelineLayout;
		VkPipeline graphicsPipeline;
};

