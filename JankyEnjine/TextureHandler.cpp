#include "TextureHandler.h"

VkResult TextureHandler::createTextureImagesAndViews(VkPhysicalDevice physicalDevice, VkDevice device, BufferHandler& bufferHandler, MemoryHandler& memoryHandler, CommandPoolHandler& commandPoolHandler, QueueHandler& queueHandler, std::vector<Model>& models)
{
	VkResult result = VK_SUCCESS;

	Reader reader;
	
	struct imageProps {
		stbi_uc* pixels;

		int texWidth, texHeight, texChannels;
		VkDeviceSize imageSize;
	};

	std::vector<imageProps> images(19);

	images[0].pixels = reader.loadTextureImage("textures/skybox.jpg", images[0].texWidth, images[0].texHeight, images[0].texChannels);
	images[1].pixels = reader.loadTextureImage("textures/wall_00.jpg", images[1].texWidth, images[1].texHeight, images[1].texChannels);
	images[2].pixels = reader.loadTextureImage("textures/wall_01.jpg", images[2].texWidth, images[2].texHeight, images[2].texChannels);
	images[3].pixels = reader.loadTextureImage("textures/wall_02.jpg", images[3].texWidth, images[3].texHeight, images[3].texChannels);
	images[4].pixels = reader.loadTextureImage("textures/wall_03.jpg", images[4].texWidth, images[4].texHeight, images[4].texChannels);
	images[5].pixels = reader.loadTextureImage("textures/floor_00.jpg", images[5].texWidth, images[5].texHeight, images[5].texChannels);
	images[6].pixels = reader.loadTextureImage("textures/floor_01.jpg", images[6].texWidth, images[6].texHeight, images[6].texChannels);
	images[7].pixels = reader.loadTextureImage("textures/floor_02.jpg", images[7].texWidth, images[7].texHeight, images[7].texChannels);
	images[8].pixels = reader.loadTextureImage("textures/floor_03.jpg", images[8].texWidth, images[8].texHeight, images[8].texChannels);
	images[9].pixels = reader.loadTextureImage("textures/floor_04.jpg", images[9].texWidth, images[9].texHeight, images[9].texChannels);
	images[10].pixels = reader.loadTextureImage("textures/ceiling_00.jpg", images[10].texWidth, images[10].texHeight, images[10].texChannels);
	images[11].pixels = reader.loadTextureImage("textures/ceiling_01.jpg", images[11].texWidth, images[11].texHeight, images[11].texChannels);
	images[12].pixels = reader.loadTextureImage("textures/ceiling_02.jpg", images[12].texWidth, images[12].texHeight, images[12].texChannels);
	images[13].pixels = reader.loadTextureImage("textures/health_potion.png", images[13].texWidth, images[13].texHeight, images[13].texChannels);
	images[14].pixels = reader.loadTextureImage("textures/container_00.png", images[14].texWidth, images[14].texHeight, images[14].texChannels);
	images[15].pixels = reader.loadTextureImage("textures/container_01.png", images[15].texWidth, images[15].texHeight, images[15].texChannels);
	images[16].pixels = reader.loadTextureImage("textures/rock_00.jpg", images[16].texWidth, images[16].texHeight, images[16].texChannels);
	images[17].pixels = reader.loadTextureImage("textures/grenade_00.png", images[17].texWidth, images[17].texHeight, images[17].texChannels);
	images[18].pixels = reader.loadTextureImage("textures/gun_00.jpg", images[18].texWidth, images[18].texHeight, images[18].texChannels);

	for (imageProps& image : images) {
		if (!image.pixels) {
			throw std::runtime_error("Failed to load texture images.");
		}

		image.imageSize = static_cast<uint32_t>(image.texWidth)* image.texHeight * 4;
	}

	size_t index = 0;
	
	textureImages.resize(images.size());
	textureImageViews.resize(images.size());
	textureImageSamplers.resize(images.size());
	memoryHandler.textureImageMemory.resize(images.size());

	for (size_t i = 0; i < images.size(); ++i) {
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;

		result = bufferHandler.createTextureBuffer(physicalDevice, device, memoryHandler, stagingBuffer, stagingBufferMemory, images[i].imageSize, images[i].pixels);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to create texture buffer." << std::endl;
			return result;
		}

		stbi_image_free(images[i].pixels);

		VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
		VkImageUsageFlags usageFlags = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

		VkImageCreateInfo imageInfo = {};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = static_cast<uint32_t>(images[i].texWidth);
		imageInfo.extent.height = static_cast<uint32_t>(images[i].texHeight);
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.format = format;
		imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = usageFlags;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

		result = vkCreateImage(device, &imageInfo, nullptr, &textureImages[i]);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to create image." << std::endl;
			return result;
		}

		result = memoryHandler.createTextureMemory(physicalDevice, device, textureImages[i], i);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to allocate texture memory." << std::endl;
			return result;
		}

		bufferHandler.transitionImageLayout(device, commandPoolHandler.getCommandPool(), queueHandler.getGraphicsQueue(),textureImages[i], VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		bufferHandler.copyBufferToImage(device, commandPoolHandler.getCommandPool(), queueHandler.getGraphicsQueue(), stagingBuffer, textureImages[i], static_cast<uint32_t>(images[i].texWidth), static_cast<uint32_t>(images[i].texHeight));
		bufferHandler.transitionImageLayout(device, commandPoolHandler.getCommandPool(), queueHandler.getGraphicsQueue(), textureImages[i], VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);

		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = textureImages[i];
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
		viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = 1;

		result = vkCreateImageView(device, &viewInfo, nullptr, &textureImageViews[i]);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to create image view" << std::endl;
			return result;
		}
	}

	for (auto& model : models) {
		model.textureImageView = &textureImageViews[model.textureId];
	}

	return result;
}

VkResult TextureHandler::createTextureSamplers(VkDevice& device, std::vector<Model>& models)
{
	VkResult result = VK_SUCCESS;
	for (size_t i = 0; i < textureImageSamplers.size(); i++) {
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 0.0f;

		result = vkCreateSampler(device, &samplerInfo, nullptr, &textureImageSamplers[i]);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create texture samplers." << std::endl;
			return result;
		}
	}

	for (Model& model : models) {
		model.textureSampler = &textureImageSamplers[model.textureId];
	}

	return result;
}

void TextureHandler::Destroy(VkDevice& device) {
	for (auto& textureImage : textureImages) {
		vkDestroyImage(device, textureImage, nullptr);
	}

	for (auto& textureImageView : textureImageViews) {
		vkDestroyImageView(device, textureImageView, nullptr);
	}

	for (auto& textureImageSampler : textureImageSamplers) {
		vkDestroySampler(device, textureImageSampler, nullptr);
	}
}
