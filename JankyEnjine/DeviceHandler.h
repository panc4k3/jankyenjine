#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>
#include <map>
#include <optional>
#include <set>

#include "VulkanHandler.h"

class DeviceHandler
{
	public:
		void setProperties(VulkanAppProperties* vulkanAppProperties);
		VkResult selectPhysicalDevice(VulkanHandler* vkHandler, VkSurfaceKHR surface, SwapChainSupportDetails& swapChainDetails);
		VkResult createLogicalDevice();
		VkDevice& getDevice();
		VkPhysicalDevice& getPhysicalDevice();
		void findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);
		void querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface, SwapChainSupportDetails& swapChainDetails);
		QueueFamilyIndices& getIndices();
		VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
		VkFormat findDepthFormat();
		bool hasStencilComponent(VkFormat format);
		void Destroy();

	private:
		VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
		VkDevice device;
		QueueFamilyIndices indices;

		VulkanAppProperties* m_vulkanAppProperties;

		int rateDeviceScore(VkPhysicalDevice device, VkSurfaceKHR surface, SwapChainSupportDetails& swapChainDetails);
		bool checkDeviceExtensionSupport(VkPhysicalDevice device);
};

