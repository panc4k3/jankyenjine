#include "VertexRoom.h"

VertexRoom::VertexRoom(glm::vec3 position, glm::vec3 color, glm::vec2 textureCoord) {
	pos = position;
	this->color = color;
	texCoord = textureCoord;
}

glm::vec3 VertexRoom::getPosition() {
	return pos;
}

glm::vec3 VertexRoom::getColor() {
	return color;
}

glm::vec2 VertexRoom::getTexCoord() {
	return texCoord;
}

VertexRoom::~VertexRoom() {
	
}