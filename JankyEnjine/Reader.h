#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "Room.h"
#include <nlohmann/json.hpp>
#include <fstream>
#include <iostream>
#include <unordered_map>

#include "Light.h"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include "stb_image.h"

class Reader
{
	public:
		bool loadModel(std::vector<Model>& models, const std::string model_name, uint32_t& index, ModelType type);
		bool loadLevel(std::vector<Room>& rooms, std::vector<Model>& models, const std::string level_name, uint32_t& index);
		void loadShaders(std::vector<char>& vertShaderCode, std::vector<char>& fragShaderCode);
		stbi_uc* loadTextureImage(const char* path_name, int& texWidth, int& texHeight, int& texChannels);

	private:
		const std::string path_to_level;
		const std::string path_to_shaders = "shaders/";
		const std::string file_name = "level.json";
		const std::string vert_shader_name = "vert.spv";
		const std::string frag_shader_name = "frag.spv";
		const std::string path_to_model = "models/chalet.obj";

		nlohmann::json room_load;

		std::vector<char> readFile(const std::string& filename);
};

