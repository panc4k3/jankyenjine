#include "Reader.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

bool Reader::loadModel(std::vector<Model>& models, const std::string model_name, uint32_t& indexOffset, ModelType type)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, model_name.c_str())) {
		std::cout << warn << " : " << err << std::endl;
		return false;
	}

	Model model = {};
	std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			Vertex vertex = {};

			vertex.pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};

			vertex.texCoord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};

			vertex.color = { 1.0f, 1.0f, 1.0f };

			if (attrib.normals.size() > 0) {
				vertex.normal = {
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				};
			}
			else {
				vertex.normal = { 0.0, 0.0, 0.0 };
			}

			if (uniqueVertices.count(vertex) == 0) {
				uniqueVertices[vertex] = static_cast<uint32_t>(model.vertices.size());
				model.vertices.push_back(vertex);
			}
			model.indices.push_back(uniqueVertices[vertex]);
		}
	}
	model.type = type;
	models.push_back(model);
	return true;
}

bool Reader::loadLevel(std::vector<Room>& rooms, std::vector<Model>& models, const std::string level_name, uint32_t& indexOffset)
{	
	std::ifstream input(level_name);

	if (input.fail()) {
		return false;
	}

	input >> room_load;

	std::vector<Light> lights;

	for (auto& room : room_load["rooms"]) {

		Room room_obj = *new Room(
			RoomDoors{ room["doors"]["north"], room["doors"]["east"], room["doors"]["south"], room["doors"]["west"] },
			glm::vec3(room["position"]["x"], room["position"]["y"], room["position"]["z"]),
			room["length"],
			room["height"],
			room["width"],
			TextureIds{ room["textures"]["wall"], room["textures"]["floor"], room["textures"]["ceiling"] },
			indexOffset,
			Light(
				{ room["light"]["position"]["x"], room["light"]["position"]["y"], room["light"]["position"]["z"] },
				glm::vec4(room["light"]["color"]["r"], room["light"]["color"]["g"], room["light"]["color"]["b"], room["light"]["color"]["w"])
			)
		);

		indexOffset = 0;
		rooms.push_back(room_obj);

		size_t index = models.size() - 1;

		for (auto model : room["models"]) {
			auto name = static_cast<std::string>(model["name"]);
			std::cout << name << std::endl;
			loadModel(models, "models/" + static_cast<std::string>(model["name"]), indexOffset, IMPORT);
			++index;
			models[index].position = { model["position"]["x"] , model["position"]["y"], model["position"]["z"] };
			models[index].rotation = { model["rotation"]["x"] , model["rotation"]["y"], model["rotation"]["z"] };
			models[index].scale = { model["scale"]["x"] , model["scale"]["y"], model["scale"]["z"] };
			models[index].light = room_obj.getLight();
			models[index].textureId = model["texture_id"];
			models[index].isAnimated = model["animate"];
			models[index].specular = model["specular"];
		}
	}

	return true;
}

void Reader::loadShaders(std::vector<char>& vertShaderCode, std::vector<char>& fragShaderCode)
{
	vertShaderCode = readFile(vert_shader_name);
	fragShaderCode = readFile(frag_shader_name);
}

stbi_uc* Reader::loadTextureImage(const char* path_name, int& texWidth, int& texHeight, int& texChannels)
{
	return stbi_load(path_name, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
}

std::vector<char> Reader::readFile(const std::string& filename) {
	std::ifstream file(filename, std::ios::ate | std::ios::binary);
	
	if (!file.is_open()) {
		throw std::runtime_error("failed to open file!");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}