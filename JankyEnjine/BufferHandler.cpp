#include "BufferHandler.h"

VkResult BufferHandler::createFramebuffers(VkDevice& device, VkExtent2D& swapChainExtent, std::vector<VkImageView>& swapChainImageViews, VkRenderPass& renderPass, VkImageView &depthImageView)
{
	VkResult result = VK_SUCCESS;
	swapChainFramebuffers.resize(swapChainImageViews.size());

	for (size_t i = 0; i < swapChainImageViews.size(); i++) {
		std::array<VkImageView, 2> attachments = {
			swapChainImageViews[i],
			depthImageView
		};

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPass;
		framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.width = swapChainExtent.width;
		framebufferInfo.height = swapChainExtent.height;
		framebufferInfo.layers = 1;

		result = vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]);
		if (result != VK_SUCCESS) {
			std::cout << "Failed to create framebuffer. @" << i << std::endl;
			return result;
		}
	}

	return result;
}

VkResult BufferHandler::createVertexBuffers(VkPhysicalDevice physicalDevice, VkDevice& device, MemoryHandler& memoryHandler, CommandPoolHandler& commandPoolHandler, QueueHandler& queueHandler, std::vector<Model>& models)
{
	VkResult result;

	for (Model& model : models) {
		VkDeviceSize bufferSize = sizeof(model.vertices[0]) * model.vertices.size();

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		result = createBuffer(
			device,
			bufferSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			stagingBuffer
		);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create staging buffer." << std::endl;
			return result;
		}

		result = memoryHandler.createBufferMemory(
			physicalDevice,
			device,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer,
			stagingBufferMemory
		);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create staging buffer memory." << std::endl;
			return result;
		}

		memoryHandler.copyVertexBufferMemory(device, bufferSize, stagingBufferMemory, model.vertices);

		result = createBuffer(
			device,
			bufferSize,
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			model.vertexBuffer
		);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create vertex buffer." << std::endl;
			return result;
		}

		result = memoryHandler.createBufferMemory(
			physicalDevice,
			device,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			model.vertexBuffer,
			model.vertexBufferMemory
		);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to create vertex buffer memory." << std::endl;
			return result;
		}

		copyBuffer(device, stagingBuffer, model.vertexBuffer, bufferSize, commandPoolHandler.getBufferCommandPool(), queueHandler.getGraphicsQueue());

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}

	return result;
}

VkResult BufferHandler::createIndexBuffers(VkPhysicalDevice physicalDevice, VkDevice& device, MemoryHandler& memoryHandler, CommandPoolHandler& commandPoolHandler, QueueHandler& queueHandler, std::vector<Model>& models)
{
	VkResult result;

	for (Model& model : models) {
		VkDeviceSize bufferSize = sizeof(model.indices[0]) * model.indices.size();

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		result = createBuffer(
			device,
			bufferSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			stagingBuffer
		);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create staging buffer." << std::endl;
			return result;
		}

		result = memoryHandler.createBufferMemory(
			physicalDevice,
			device,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer,
			stagingBufferMemory
		);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create staging buffer memory." << std::endl;
			return result;
		}

		memoryHandler.copyIndiceBufferMemory(device, bufferSize, stagingBufferMemory, model.indices);

		result = createBuffer(
			device,
			bufferSize,
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			model.indexBuffer
		);

		if (result != VK_SUCCESS) {
			std::cout << "Unable to create index buffer." << std::endl;
			return result;
		}

		result = memoryHandler.createBufferMemory(
			physicalDevice,
			device,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			model.indexBuffer,
			model.indexBufferMemory
		);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to create vertex buffer memory." << std::endl;
			return result;
		}

		copyBuffer(device, stagingBuffer, model.indexBuffer, bufferSize, commandPoolHandler.getBufferCommandPool(), queueHandler.getGraphicsQueue());

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}

	return result;
}

VkResult BufferHandler::createCommandBuffers(VkDevice& device, VkExtent2D& swapChainExtent, VkPipeline& graphicsPipeline, VkPipelineLayout& pipelineLayout, VkRenderPass& renderPass, VkCommandPool& commandPool, const std::vector<Model>& models)
{
	VkResult result;
	commandBuffers.resize(swapChainFramebuffers.size());

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

	result = vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data());

	if (result != VK_SUCCESS) {
		std::cout << "Failed to allocate command buffers." << std::endl;
		return result;
	}

	for (size_t i = 0; i < commandBuffers.size(); i++) {
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		//beginInfo.flags = 0; // Optional
		//beginInfo.pInheritanceInfo = nullptr; // Optional

		/*
			Vk Start recording command
		*/

		result = vkBeginCommandBuffer(commandBuffers[i], &beginInfo);

		if (result != VK_SUCCESS) {
			std::cout << "Failed to begin recording command buffer." << std::endl;
			return result;
		}

		std::array<VkClearValue, 2> clearValues = {};
		clearValues[0].color = CLEAR_COLOR;
		clearValues[1].depthStencil = CLEAR_DEPTH_STENCIL;

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = renderPass;
		renderPassInfo.framebuffer = swapChainFramebuffers[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;
		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassInfo.pClearValues = clearValues.data();

		/*
			Start render pass
		*/

		vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

		/*
			Bind the graphics pipeline
		*/

		vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

		/*
			Bind Buffers and descriptor sets
		*/

		VkDeviceSize offsets[] = { 0 };

		for (size_t j = 0; j < models.size(); j++) {
			vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, &models[j].vertexBuffer, offsets);
			vkCmdBindIndexBuffer(commandBuffers[i], models[j].indexBuffer, 0, VK_INDEX_TYPE_UINT32);

			vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &models[j].descriptorSet, 0, nullptr);

			vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(models[j].indices.size()), 1, 0, 0, 0);
		}

		/*
			Draw call

			@param 2: indices count
			@param 3: instance count
			@param 4: first index
			@param 5: offset into index buffer
			@param 5: first instance
		*/

		

		/*
			End render pass
		*/

		vkCmdEndRenderPass(commandBuffers[i]);

		/*
			Vk End command recording
		*/

		result = vkEndCommandBuffer(commandBuffers[i]);

		if (result != VK_SUCCESS) {
			std::cout << "Failed to record command buffer." << std::endl;
			return result;
		}
	}

	return VK_SUCCESS;
}

VkResult BufferHandler::createUniformBuffers(VkPhysicalDevice physicalDevice, VkDevice device, MemoryHandler& memoryHandler, size_t swapChainImagesSize, std::vector<Model>& models)
{
	VkResult result = VK_SUCCESS;
	VkDeviceSize bufferSize = sizeof(UniformBufferObject);

	for (Model& model : models) {
		result = createBuffer(device, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, model.uniformBuffer);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to create uniform buffer." << std::endl;
			return result;
		}

		result = memoryHandler.createBufferMemory(physicalDevice, device, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, model.uniformBuffer, model.uniformBufferMemory);
		if (result != VK_SUCCESS) {
			std::cout << "Unable to allocate uniform buffer memory" << std::endl;
			return result;
		}
	}

	return result;
}

VkResult BufferHandler::createTextureBuffer(VkPhysicalDevice physicalDevice, VkDevice& device, MemoryHandler memoryHandler, VkBuffer& stagingBuffer, VkDeviceMemory& stagingBufferMemory, VkDeviceSize& imageSize, stbi_uc* pixels)
{
	VkResult result;

	result = createBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, stagingBuffer);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to create texture buffer." << std::endl;
		return result;
	}

	result = memoryHandler.createBufferMemory(physicalDevice, device, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to allocate texture buffer memory." << std::endl;
		return result;
	}

	memoryHandler.copyTextureBufferMemory(device, imageSize, stagingBufferMemory, pixels);

	return result;
}

std::vector<VkCommandBuffer>& BufferHandler::getCommandBuffers()
{
	return commandBuffers;
}

void BufferHandler::transitionImageLayout(VkDevice& device, VkCommandPool& commandPool, VkQueue& graphicsQueue, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
	VkResult result;
	VkCommandBuffer commandBuffer;
	result = beginSingleTimeCommands(device, commandPool, commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to begin command buffer.");
	}

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	VkPipelineStageFlags sourceStage;
	VkPipelineStageFlags destinationStage;

	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else {
		throw std::invalid_argument("unsupported layout transition!");
	}

	vkCmdPipelineBarrier(
		commandBuffer,
		sourceStage, destinationStage,
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	result = endSingleTimeCommands(device, graphicsQueue, commandPool, commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to begin command buffer.");
	}
}

void BufferHandler::copyBufferToImage(VkDevice& device, VkCommandPool& commandPool, VkQueue& graphicsQueue, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
	VkResult result;
	VkCommandBuffer commandBuffer;
	result = beginSingleTimeCommands(device, commandPool, commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to begin command buffer.");
	}

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;

	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;

	region.imageOffset = { 0, 0, 0 };
	region.imageExtent = {
		width,
		height,
		1
	};

	vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

	result = endSingleTimeCommands(device, graphicsQueue, commandPool, commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to begin command buffer.");
	}
}

void BufferHandler::copyBuffer(VkDevice& device, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkCommandPool& commandPool, VkQueue& graphicsQueue)
{
	VkResult result;
	VkCommandBuffer commandBuffer;
	result = beginSingleTimeCommands(device, commandPool, commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to begin command buffer.");
	}

	VkBufferCopy copyRegion = {};
	copyRegion.srcOffset = 0; // Optional
	copyRegion.dstOffset = 0; // Optional
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

	result = endSingleTimeCommands(device, graphicsQueue, commandPool, commandBuffer);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to begin command buffer.");
	}
}

VkResult BufferHandler::beginSingleTimeCommands(VkDevice& device, VkCommandPool& commandPool, VkCommandBuffer& commandBuffer) {
	VkResult result;
	
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = commandPool;
	allocInfo.commandBufferCount = 1;

	result = vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to allocate command buffers." << std::endl;
		return result;
	}

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	result = vkBeginCommandBuffer(commandBuffer, &beginInfo);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to begin command buffer." << std::endl;
		return result;
	}

	return result;
}

VkResult BufferHandler::endSingleTimeCommands(VkDevice& device, VkQueue& graphicsQueue, VkCommandPool& commandPool, VkCommandBuffer& commandBuffer) {
	VkResult result = vkEndCommandBuffer(commandBuffer);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to end command buffer." << std::endl;
		return result;
	}

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	result = vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to submit buffer to queue." << std::endl;
		return result;
	}
	
	vkQueueWaitIdle(graphicsQueue);

	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);

	return result;
}

void BufferHandler::Destroy(VkDevice& device)
{
	DestroyFramebuffers(device);
}

void BufferHandler::DestroyFramebuffers(VkDevice& device)
{
	for (auto framebuffer : swapChainFramebuffers) {
		vkDestroyFramebuffer(device, framebuffer, nullptr);
	}

	swapChainFramebuffers.clear();
}

void BufferHandler::FreeCommandBuffers(VkDevice& device, VkCommandPool commandPool)
{
	vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
	commandBuffers.clear();
}

uint32_t BufferHandler::findMemoryType(VkPhysicalDevice& physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}

	throw std::runtime_error("failed to find suitable memory type!");
}

VkResult BufferHandler::createBuffer(VkDevice& device, VkDeviceSize size, VkBufferUsageFlags usage, VkBuffer& buffer)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	return vkCreateBuffer(device, &bufferInfo, nullptr, &buffer);
}
