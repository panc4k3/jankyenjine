// JankyEnjine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <atomic>
#include <thread>

#include "Room.h"
#include "RoomMap.h"
#include <glm/glm.hpp>
#include <iostream>
#include "Reader.h"
#include "Writer.h"
#include "Engine.h"

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

const int WINDOW_WIDTH = 1920;
const int WINDOW_HEIGHT = 1080;

const std::string COMMAND_TRIANGLES = "tris";
const std::string COMMAND_QUIT = "quit";
const std::string COMMAND_DRAW_FPS = "fps";
const std::string COMMAND_LOAD_LEVEL = "load";
const std::string COMMAND_LOAD_MODEL = "spawn";
const std::string COMMAND_HELP = "help";

EngineProperties engineProperties = {
		WINDOW_WIDTH,
		WINDOW_HEIGHT,
		"JankyEnjine",
		"JankyEnjine",
		"No Engine",
		enableValidationLayers,
		{"VK_LAYER_KHRONOS_validation"},
		{VK_KHR_SWAPCHAIN_EXTENSION_NAME}
};

void InputThread(std::atomic<int>& args, std::string& value) {
	std::string buffer;
	while (args.load()) {
		std::cin >> buffer;
		
		if (buffer == COMMAND_QUIT) {
			args.store(Quit);
		}
		else if (buffer == COMMAND_DRAW_FPS) {
			args.store(DrawFps);
		}
		else if (buffer == COMMAND_LOAD_LEVEL) {
			std::cin >> buffer;
			if (buffer == "-n") {
				std::cin >> buffer;
				if (buffer != "") {
					value = buffer;
					args.store(LoadLevel);
				}
				else {
					args.store(NotFound);
				}
			}
			else {
				args.store(NotFound);
			}
		}
		else if (buffer == COMMAND_TRIANGLES) {
			args.store(TriangleCount);
		}
		else if (buffer == COMMAND_LOAD_MODEL) {
			std::cin >> buffer;
			if (buffer == "-n") {
				std::cin >> buffer;
				if (buffer != "") {
					value = buffer;
					args.store(LoadModel);
				}
				else
				{
					args.store(NotFound);
				}
			}
			else
			{
				args.store(NotFound);
			}
		}
		else if (buffer == COMMAND_HELP) {
			args.store(Help);
		}
		else {
			args.store(NotFound);
		}

		buffer = "";
	}
}

void LoadEngineFunc(Engine& engine, std::vector<Room>& rooms, std::vector<Model>& models) {
	try {
		engine.Init(rooms, models);
		engine.Start();
	}
	catch (const std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}
}

void UpdateBufferFunc(Engine& engine, std::vector<Room>& rooms, std::vector<Model>& models) {
	try {
		engine.Reset(rooms, models);
	}
	catch (const std::exception & e) {
		std::cerr << e.what() << std::endl;
	}
}

void UpdateEngineFunc(Engine& engine, std::atomic<int>& args) {
	try {
		engine.Update(args);
	}
	catch (const std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}
}

void StopEngineFunc(Engine& engine, std::atomic<int>& args) {
	try {
		engine.Destroy();
		args.store(InitEngine);
	}
	catch (const std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}
}

void DisplayHelp() {
	std::cout << "Command list:\n" <<
		"quit -> Exits the application\n" <<
		"tris -> Displays amount of triangles in level.\n" <<
		"fps -> Displays the current fps count.\n" <<
		"load -n <level name> -> Loads a level by name.\n" <<
		"spawn -n <model name> -> Spawns a model by name." << std::endl;
}

int main(int argc, char **argv)
{
	Engine engine;
	engine.setProperties(&engineProperties);

	std::vector<Room> rooms;
	std::vector<Model> models;
	uint32_t index = 0;

	Reader reader;
	bool updateEngine = false;
	std::string value;
	std::atomic<int> inputArgs(Continue);
	std::thread inputThread(InputThread, std::ref(inputArgs), std::ref(value));

	do {
		switch (inputArgs.load())
		{
		case NotFound:
			std::cout << "Command not found!" << std::endl;
			inputArgs.store(Continue);
			break;

		case Help:
			DisplayHelp();
			inputArgs.store(Continue);
			break;

		case DrawFps:
			std::cout << "FPS: " << engine.getCurrentFPS() << std::endl;
			inputArgs.store(Continue);
			break;

		case TriangleCount:
			std::cout << "Triangle Count: " << engine.getTriangleCount() << std::endl;
			inputArgs.store(Continue);
			break;

		case LoadLevel:
			if (reader.loadModel(models, "models/skybox.obj", index, SKYBOX) && reader.loadLevel(rooms, models, value, index)) {
				LoadEngineFunc(engine, rooms, models);
				updateEngine = true;
			}
			else {
				std::cout << "Level not found!" << std::endl;
			}
			value = "";
			inputArgs.store(Continue);
			break;

		case LoadModel:
			if (reader.loadModel(models, "models/" + value, index, IMPORT)) {
				UpdateBufferFunc(engine, rooms, models);
				inputArgs.store(Continue);
			}
			else
			{
				std::cout << "Model not found!" << std::endl;
			}
			value = "";
			inputArgs.store(Continue);
			break;

		case StopEngine:
			updateEngine = false;
			StopEngineFunc(engine, inputArgs);
			break;

		case Quit:
			if (updateEngine) {
				StopEngineFunc(engine, inputArgs);
			}
			break;

		default:
			break;
		}

		if (updateEngine) {
			UpdateEngineFunc(engine, inputArgs);
		}
	} while (inputArgs.load() != Quit);

	inputThread.join();

	return EXIT_SUCCESS;
}