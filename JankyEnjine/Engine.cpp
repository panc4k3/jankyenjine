#include "Engine.h"

static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
	auto app = reinterpret_cast<Engine*>(glfwGetWindowUserPointer(window));
	app->framebufferResized = true;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	auto app = reinterpret_cast<Engine*>(glfwGetWindowUserPointer(window));
	glm::vec4& lightsEnabled = app->getLightsEnabled();

	if (key ==  GLFW_KEY_V && action == GLFW_PRESS) {
		if (lightsEnabled.x == 1) {
			lightsEnabled = glm::vec4(0, lightsEnabled.y, lightsEnabled.z, lightsEnabled.w);
		}
		else {
			lightsEnabled = glm::vec4(1, lightsEnabled.y, lightsEnabled.z, lightsEnabled.w);
		}
	}

	if (key == GLFW_KEY_B && action == GLFW_PRESS) {
		if (lightsEnabled.y == 1) {
			lightsEnabled = glm::vec4(lightsEnabled.x, 0, lightsEnabled.z, lightsEnabled.w);
		}
		else {
			lightsEnabled = glm::vec4(lightsEnabled.x, 1, lightsEnabled.z, lightsEnabled.w);
		}
	}
	
	if (key == GLFW_KEY_N && action == GLFW_PRESS) {
		if (lightsEnabled.z == 1) {
			lightsEnabled = glm::vec4(lightsEnabled.x, lightsEnabled.y, 0, lightsEnabled.w);
		}
		else {
			lightsEnabled = glm::vec4(lightsEnabled.x, lightsEnabled.y, 1, lightsEnabled.w);
		}
	}
}

void Engine::Init(std::vector<Room>& rooms, std::vector<Model>& models)
{
	for (Model& model : models) {
		model.prepare();
		modelsObjects.push_back(model);
	}

	for (Room& room : rooms) {
		for (auto& part : room.parts) {
			Model model = { part.getVertices(), part.getIndices(), room.getLight(), part.getTextureId(), ROOM, 8 };
			model.prepare();
			modelsObjects.push_back(model);
		}
	}

	initWindow();
	initVulkan();
}

void Engine::Reset(std::vector<Room>& rooms, std::vector<Model>& models)
{
	VkDevice& device = deviceHandler.getDevice();
	vkDeviceWaitIdle(device);

	modelsObjects.clear();

	for (Model& model : models) {
		model.ubo.model = glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model.ubo.model = glm::scale(model.ubo.model, glm::vec3(1000.0f, 1000.0f, 1000.0f));
		modelsObjects.push_back(model);
	}

	for (Room& room : rooms) {
		for (auto& part : room.parts) {
			Model model = { part.getVertices(), part.getIndices(), room.getLight(), part.getTextureId() };
			modelsObjects.push_back(model);
		} 
	}

	if (bufferHandler.createVertexBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, commandPoolHandler, queueHandler, modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create vertex buffer.");
	}

	if (bufferHandler.createIndexBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, commandPoolHandler, queueHandler, modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create index buffer.");
	}

	if (bufferHandler.createUniformBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, swapChainHandler.getSwapChainImages().size(), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create uniform buffers.");
	}

	if (descriptorHandler.createDescriptorPool(device, static_cast<uint32_t>(modelsObjects.size())) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor pool.");
	}

	if (descriptorHandler.createDescriptorSets(device, static_cast<uint32_t>(swapChainHandler.getSwapChainImages().size()), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor sets.");
	}

	if (bufferHandler.createCommandBuffers(device, swapChainHandler.getSwapChainExtent(), pipelineHandler.getGraphicsPipeline(), pipelineHandler.getPipelineLayout(), renderPassHandler.getRenderPass(), commandPoolHandler.getCommandPool(), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Uanble to create command buffers.");
	}
}

void Engine::Start()
{

}

void Engine::Update(std::atomic<int>& args)
{
	auto frameEndTime = std::chrono::high_resolution_clock::now();

	if (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		drawFrame();
	}
	else
	{
		args.store(StopEngine);
	}

	vkDeviceWaitIdle(deviceHandler.getDevice());
}

void Engine::Destroy()
{
	VkDevice& device = deviceHandler.getDevice();
	
	syncHandler.Destroy(device);

	commandPoolHandler.Destroy(device);

	bufferHandler.Destroy(device);

	memoryHandler.FreeMemory(device);

	pipelineHandler.Destroy(device);

	renderPassHandler.Destroy(device);

	swapChainHandler.Destroy(device);

	deviceHandler.Destroy();

	surfaceHandler.Destroy(vulkanHandler.getInstance());

	vulkanHandler.Destroy();

	glfwDestroyWindow(window);
	glfwTerminate();
}

void Engine::setProperties(EngineProperties *engineProperties)
{
	m_engineProperties = engineProperties;
	m_vulkanProperties.APPLICATION_NAME = engineProperties->APP_NAME;
	m_vulkanProperties.ENGINE_NAME = engineProperties->ENGINE_NAME;
	m_vulkanProperties.VALIDATION_LAYERS = engineProperties->VALIDATION_LAYERS;
	m_vulkanProperties.ENABLE_VALIDATION_LAYERS = engineProperties->ENABLE_VALIDATION_LAYERS;
	m_vulkanProperties.DEVICE_EXTENSIONS = engineProperties->DEVICE_EXTENSIONS;
}

int Engine::getCurrentFPS()
{
	double time = frameCurrentTime - frameEndTime;
	assert(time != 0.0f);
	return static_cast<int>(1.0f / time);
}

size_t Engine::getTriangleCount()
{
	/*return indices.size() / 3;*/
	size_t tris = 0;
	for (Model model : modelsObjects) {
		tris += model.indices.size() / 3;
	}
	return tris;
}

glm::vec4& Engine::getLightsEnabled()
{
	return lightsEnabled;
}

BufferHandler& Engine::getBufferHandler()
{
	return bufferHandler;
}

void Engine::initVulkan()
{
	vulkanHandler.setProperties(&m_vulkanProperties);
	deviceHandler.setProperties(&m_vulkanProperties);

	if (vulkanHandler.createInstance() != VK_SUCCESS) {
		throw std::runtime_error("Unable to create Vulkan instance.");
	}

	if (m_engineProperties->ENABLE_VALIDATION_LAYERS) {
		if (vulkanHandler.setupDebugMessenger() != VK_SUCCESS) {
			throw std::runtime_error("Unable to setup debug messenger.");
		}
	}

	VkSurfaceKHR& surface = surfaceHandler.getSurface();

	if (glfwCreateWindowSurface(vulkanHandler.getInstance(), window, nullptr, &surface) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create window surface.");
	}
	
	if (deviceHandler.selectPhysicalDevice(&vulkanHandler, surface, swapChainHandler.getSwapChainDetails()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to select device");
	}

	if (deviceHandler.createLogicalDevice() != VK_SUCCESS) {
		throw std::runtime_error("Unable to create logical device.");
	}

	queueHandler.setupQueues(deviceHandler);

	VkDevice& device = deviceHandler.getDevice();

	if (swapChainHandler.createSwapChain(window, surfaceHandler.getSurface(), deviceHandler) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create swap chain.");
	}

	swapChainHandler.setSwapChainImages(device);

	if (swapChainHandler.createImageViews(device) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create swap chain image views.");
	}

	if (renderPassHandler.createRenderPass(device, deviceHandler, swapChainHandler.getSwapChainImageFormat()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create render pass.");
	}

	VkRenderPass& renderPass = renderPassHandler.getRenderPass();
	
	if (descriptorHandler.createDescriptorSetLayouts(device) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor set layout.");
	}

	if (pipelineHandler.createGraphicsPipeline(device, swapChainHandler.getSwapChainExtent(), renderPass, descriptorHandler.getDescriptorSetLayouts()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create graphics pipeline.");
	}

	if (commandPoolHandler.createCommandPool(device, deviceHandler.getIndices()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create command pool.");
	}

	if (commandPoolHandler.createBufferCommandPool(device, deviceHandler.getIndices()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create buffer command pool.");
	}

	if (swapChainHandler.createDepthResources(deviceHandler.getPhysicalDevice(), device, deviceHandler, memoryHandler) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create depth resources.");
	}

	if (bufferHandler.createFramebuffers(device, swapChainHandler.getSwapChainExtent(), swapChainHandler.getSwapChainImageViews(), renderPass, swapChainHandler.getDepthImageView()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create frame buffers.");
	}

	if (textureHandler.createTextureImagesAndViews(deviceHandler.getPhysicalDevice(), device, bufferHandler, memoryHandler, commandPoolHandler, queueHandler, modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create texture image.");
	}

	if (textureHandler.createTextureSamplers(device, modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create texture sampler.");
	}

	if (bufferHandler.createVertexBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, commandPoolHandler, queueHandler, modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create vertex buffer.");
	}

	if (bufferHandler.createIndexBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, commandPoolHandler, queueHandler, modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create index buffer.");
	}

	if (bufferHandler.createUniformBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, swapChainHandler.getSwapChainImages().size(), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create uniform buffers.");
	}

	if (descriptorHandler.createDescriptorPool(device, static_cast<uint32_t>(modelsObjects.size())) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor pool.");
	}

	if (descriptorHandler.createDescriptorSets(device, static_cast<uint32_t>(swapChainHandler.getSwapChainImages().size()), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor sets.");
	}

	if (bufferHandler.createCommandBuffers(device, swapChainHandler.getSwapChainExtent(), pipelineHandler.getGraphicsPipeline(), pipelineHandler.getPipelineLayout(), renderPass, commandPoolHandler.getCommandPool(), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Uanble to create command buffers.");
	}
	if (syncHandler.createSemaphores(device) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create semaphores.");
	}

	if (syncHandler.createFences(device) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create fences.");
	}
}

void Engine::initWindow()
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	//glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = glfwCreateWindow(m_engineProperties->WINDOW_WIDTH, m_engineProperties->WINDOW_HEIGHT, m_engineProperties->WINDOW_NAME, nullptr, nullptr);
	glfwSetWindowUserPointer(window, this);
	glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
	glfwSetKeyCallback(window, key_callback);

	if (glfwRawMouseMotionSupported())
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
		glfwGetCursorPos(window, &xpos, &ypos);

	Camera = glm::translate(glm::mat4(1.0f), glm::vec3(-2.0f, 0.0f, -1.0f)) * Camera;
	Camera = glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(1.0f, 0.0f, 0.0f)) * Camera;
	//Camera = glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)) * Camera;
}

void Engine::drawFrame()
{
	frameEndTime = frameCurrentTime;
	frameCurrentTime = glfwGetTime();
	VkDevice& device = deviceHandler.getDevice();

	vkWaitForFences(device, 1, &syncHandler.getInFlightFences()[currentFrame], VK_TRUE, UINT64_MAX);

	uint32_t imageIndex = 0;

	VkResult result = vkAcquireNextImageKHR(device, swapChainHandler.getSwapChain(), UINT64_MAX, syncHandler.getImageAvaliableSemaphores()[currentFrame], VK_NULL_HANDLE, &imageIndex);

	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		updateSwapChain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		throw std::runtime_error("Failed to acquire swap chain image.");
	}

	updateUniformBuffer(imageIndex);

	//vkAcquireNextImageKHR(device, swapChainHandler.getSwapChain(), UINT64_MAX, syncHandler.getImageAvaliableSemaphores()[currentFrame], VK_NULL_HANDLE, &imageIndex);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = { syncHandler.getImageAvaliableSemaphores()[currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &bufferHandler.getCommandBuffers()[imageIndex];

	VkSemaphore signalSemaphores[] = { syncHandler.getRenderFinishedSemaphores()[currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	vkResetFences(device, 1, &syncHandler.getInFlightFences()[currentFrame]);

	if (vkQueueSubmit(queueHandler.getGraphicsQueue(), 1, &submitInfo, syncHandler.getInFlightFences()[currentFrame]) != VK_SUCCESS) {
		throw std::runtime_error("Failed to submit draw command buffer.");
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;
	VkSwapchainKHR swapChains[] = { swapChainHandler.getSwapChain() };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;

	result = vkQueuePresentKHR(queueHandler.getPresentQueue(), &presentInfo);

	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized) {
		framebufferResized = false;
		updateSwapChain();

	} else if (result != VK_SUCCESS) {
		throw std::runtime_error("Unable to present swap chain image.");
	}

	currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Engine::createSwapChainAndDependencies() {
	VkDevice& device = deviceHandler.getDevice();

	if (swapChainHandler.createSwapChain(window, surfaceHandler.getSurface(), deviceHandler) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create swap chain.");
	}

	swapChainHandler.setSwapChainImages(device);

	if (swapChainHandler.createImageViews(device) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create swap chain image views.");
	}

	if (renderPassHandler.createRenderPass(device, deviceHandler, swapChainHandler.getSwapChainImageFormat()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create render pass.");
	}

	VkRenderPass& renderPass = renderPassHandler.getRenderPass();

	if (pipelineHandler.createGraphicsPipeline(device, swapChainHandler.getSwapChainExtent(), renderPass, descriptorHandler.getDescriptorSetLayouts()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create graphics pipeline.");
	}

	if (swapChainHandler.createDepthResources(deviceHandler.getPhysicalDevice(), device, deviceHandler, memoryHandler) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create depth resources.");
	}

	if (bufferHandler.createFramebuffers(device, swapChainHandler.getSwapChainExtent(), swapChainHandler.getSwapChainImageViews(), renderPass, swapChainHandler.getDepthImageView()) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create frame buffers.");
	}

	if (bufferHandler.createUniformBuffers(deviceHandler.getPhysicalDevice(), device, memoryHandler, swapChainHandler.getSwapChainImages().size(), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create uniform buffers.");
	}

	if (descriptorHandler.createDescriptorPool(device, static_cast<uint32_t>(modelsObjects.size())) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor pool.");
	}

	if (descriptorHandler.createDescriptorSets(device, static_cast<uint32_t>(swapChainHandler.getSwapChainImages().size()), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Unable to create descriptor sets.");
	}

	if (bufferHandler.createCommandBuffers(device, swapChainHandler.getSwapChainExtent(), pipelineHandler.getGraphicsPipeline(), pipelineHandler.getPipelineLayout(), renderPass, commandPoolHandler.getCommandPool(), modelsObjects) != VK_SUCCESS) {
		throw std::runtime_error("Uanble to create command buffers.");
	}
}

void Engine::updateSwapChain()
{
	int width = 0, height = 0;
	while (width == 0 || height == 0) {
		glfwGetFramebufferSize(window, &width, &height);
		glfwWaitEvents();
	}

	vkDeviceWaitIdle(deviceHandler.getDevice());

	destroySwapChain();
	createSwapChainAndDependencies();
}

void Engine::destroySwapChain() {
	VkDevice& device = deviceHandler.getDevice();

	memoryHandler.freeDepthImageMemory(device);
	bufferHandler.DestroyFramebuffers(device);
	bufferHandler.FreeCommandBuffers(device, commandPoolHandler.getCommandPool());
	for (Model& model : modelsObjects) {
		model.Destroy(device);
	}
	descriptorHandler.destroyDescriptorPool(device);
	pipelineHandler.Destroy(device);
	renderPassHandler.Destroy(device);
	swapChainHandler.Destroy(device);
}

void Engine::updateUniformBuffer(uint32_t currentImage)
{
	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		Camera = glm::rotate(glm::mat4(1.0f), -movementSpeed, glm::vec3(0.0f, 1.0f, 0.0f)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		Camera = glm::rotate(glm::mat4(1.0f), movementSpeed, glm::vec3(0.0f, 1.0f, 0.0f)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		Camera = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, movementSpeed, 0.0f)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
		Camera = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -movementSpeed, 0.0f)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		Camera = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, movementSpeed)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		Camera = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -movementSpeed)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		Camera = glm::translate(glm::mat4(1.0f), glm::vec3(movementSpeed, 0.0f, 0.0f)) * Camera;
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		Camera = glm::translate(glm::mat4(1.0f), glm::vec3(-movementSpeed, 0.0f, 0.0f)) * Camera;
	}

	double xposNew, yposNew;
	
	glfwGetCursorPos(window, &xposNew, &yposNew);

	xpos = xposNew;
	ypos = yposNew;

	glfwSetCursorPos(window, swapChainHandler.getSwapChainExtent().height / 2, swapChainHandler.getSwapChainExtent().width / 2);

	size_t index = 0;

	for (Model& model : modelsObjects) {
		switch (model.type) {
		case SKYBOX:
			model.ubo.lightsEnabled = lightsSkybox;
			model.ubo.model = glm::rotate(glm::mat4(1.0f), glm::radians(270.0f), glm::vec3(1.0f, 0.0f, 0.0f));
			model.ubo.model = glm::scale(model.ubo.model, glm::vec3(1000.0f, 1000.0f, 1000.0f));
			model.ubo.proj = glm::perspective(glm::radians(70.0f), swapChainHandler.getSwapChainExtent().width / (float)swapChainHandler.getSwapChainExtent().height, 0.1f, 1200.0f);
			break;

		case IMPORT:
			model.ubo.model = glm::mat4(1.0f);
			if (model.isAnimated) {
				model.ubo.model = glm::translate(model.ubo.model, { model.position.x, model.position.y, 1.2 + (glm::sin(time * 6) / 8.0f) });
			}
			else {
				model.ubo.model = glm::translate(model.ubo.model, model.position);
			}
			
			model.ubo.model = glm::rotate(model.ubo.model, glm::radians(model.rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
			model.ubo.model = glm::rotate(model.ubo.model, glm::radians(model.rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
			model.ubo.model = glm::rotate(model.ubo.model, glm::radians(model.rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));

			model.ubo.model = glm::scale(model.ubo.model, model.scale);
			model.ubo.lightsEnabled = lightsEnabled;
			model.ubo.proj = glm::perspective(glm::radians(70.0f), swapChainHandler.getSwapChainExtent().width / (float)swapChainHandler.getSwapChainExtent().height, 0.1f, 120.0f);
			break;

		case ROOM:
			model.ubo.model = glm::mat4(1.0f);
			model.ubo.lightsEnabled = lightsEnabled;
			model.ubo.proj = glm::perspective(glm::radians(70.0f), swapChainHandler.getSwapChainExtent().width / (float)swapChainHandler.getSwapChainExtent().height, 0.1f, 120.0f);
			break;

		default:
			break;
		}
		
		glm::mat3 rotMat(Camera);
		glm::vec3 d(Camera[3]);

		model.ubo.cameraPos = -d * rotMat;
		model.ubo.lightPos = model.light.getLightPos();
		model.ubo.lightColor = model.light.getLightColor();
		model.ubo.view = Camera;

		memoryHandler.copyUniformBufferMemory(deviceHandler.getDevice(), model.uniformBufferMemory, model.ubo);
		++index;
	}
}
