#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "DeviceHandler.h"

class QueueHandler
{
	public:
		void setupQueues(DeviceHandler& deviceHandler);
		VkQueue& getGraphicsQueue();
		VkQueue& getPresentQueue();

	private:
		VkQueue graphicsQueue;
		VkQueue presentQueue;
};

