#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include "StructsHandler.h"
#include "Reader.h"

class MemoryHandler
{
public:
	void copyVertexBufferMemory(VkDevice& device, VkDeviceSize& bufferSize, VkDeviceMemory& bufferMemory, const std::vector<Vertex>& vertices);
	void copyIndiceBufferMemory(VkDevice& device, VkDeviceSize& bufferSize, VkDeviceMemory& bufferMemory, const std::vector<uint32_t>& indices);
	void copyUniformBufferMemory(VkDevice& device, VkDeviceMemory& bufferMemory, UniformBufferObject& ubo);
	void copyTextureBufferMemory(VkDevice& device, VkDeviceSize bufferSize, VkDeviceMemory& bufferMemory, stbi_uc* pixels);
	VkResult createBufferMemory(VkPhysicalDevice& physicalDevice, VkDevice& device, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);
	VkResult createTextureMemory(VkPhysicalDevice& physicalDevice, VkDevice& device, VkImage& textureImage, size_t index);
	VkResult createDepthImageMemory(VkPhysicalDevice& physicalDevice, VkDevice& device, VkImage& depthImage);
	void freeTextureImageMemory(VkDevice& device);
	void freeDepthImageMemory(VkDevice& device);
	void FreeMemory(VkDevice& device);

	std::vector<VkDeviceMemory> textureImageMemory;

private:
	VkDeviceMemory depthImageMemory;

	uint32_t findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);

};
