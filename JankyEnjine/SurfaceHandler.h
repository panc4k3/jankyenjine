#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

class SurfaceHandler
{
	public:
		VkSurfaceKHR& getSurface();
		void Destroy(VkInstance& instance);

	private:
		VkSurfaceKHR surface;

};

