#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "SwapChainHandler.h"

class RenderPassHandler
{
	public:
		VkResult createRenderPass(VkDevice& device, DeviceHandler& deviceHandler, VkFormat& swapChainImageFormat);
		VkRenderPass& getRenderPass();
		void Destroy(VkDevice& device);

	private:
		VkRenderPass renderPass;
};
