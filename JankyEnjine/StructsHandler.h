#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include <optional>
#include <array>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

#include "Light.h"

struct EngineProperties
{
	int WINDOW_WIDTH;
	int WINDOW_HEIGHT;

	const char* WINDOW_NAME;
	const char* APP_NAME;
	const char* ENGINE_NAME;

	bool ENABLE_VALIDATION_LAYERS;
	std::vector<const char*> VALIDATION_LAYERS;
	std::vector<const char*> DEVICE_EXTENSIONS;
};

struct VulkanAppProperties
{
	const char* APPLICATION_NAME;
	const char* ENGINE_NAME;

	bool ENABLE_VALIDATION_LAYERS;
	std::vector<const char*> VALIDATION_LAYERS;
	std::vector<const char*> DEVICE_EXTENSIONS;
};

struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;

	bool isComplete() {
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

struct Vertex {
	glm::vec3 pos;
	glm::vec3 color;
	glm::vec2 texCoord;
	glm::vec3 normal;

	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Vertex);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	}

	static std::array<VkVertexInputAttributeDescription, 4> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 4> attributeDescriptions = {};
		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[0].offset = offsetof(Vertex, pos);

		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset = offsetof(Vertex, color);

		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[2].offset = offsetof(Vertex, texCoord);

		attributeDescriptions[3].binding = 0;
		attributeDescriptions[3].location = 3;
		attributeDescriptions[3].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[3].offset = offsetof(Vertex, normal);

		return attributeDescriptions;
	}

	bool operator==(const Vertex& other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord && normal == other.normal;
	}
};

namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
				(hash<glm::vec2>()(vertex.texCoord) << 1) ^
				(hash<glm::vec3>()(vertex.normal) << 1);
		}
	};
}

struct UniformBufferObject {
	alignas(16) glm::mat4 model;
	alignas(16) glm::mat4 view;
	alignas(16) glm::mat4 proj;
	alignas(16) glm::vec3 cameraPos;
	alignas(16) glm::vec3 lightPos;
	alignas(16) glm::vec4 lightColor;
	alignas(16) glm::vec4 lightsEnabled;
	alignas(16) float specular;
};

enum ModelType {
	SKYBOX = 0x0,
	ROOM = 0x1,
	IMPORT = 0x2
};

struct Model {
	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;

	Light light;
	size_t textureId;

	ModelType type;
	float specular;
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;
	bool isAnimated;

	VkDescriptorSet descriptorSet;
	VkBuffer uniformBuffer;
	VkBuffer vertexBuffer;
	VkBuffer indexBuffer;

	VkDeviceMemory uniformBufferMemory;
	VkDeviceMemory vertexBufferMemory;
	VkDeviceMemory indexBufferMemory;
	
	VkImageView* textureImageView;
	VkSampler* textureSampler;

	UniformBufferObject ubo;

	void prepare() {
		ubo.lightPos = light.getLightPos();
		ubo.lightColor = light.getLightColor();
		ubo.specular = specular;
	}

	void Destroy(VkDevice& device) {
		vkDestroyBuffer(device, uniformBuffer, nullptr);
		vkDestroyBuffer(device, vertexBuffer, nullptr);
		vkDestroyBuffer(device, indexBuffer, nullptr);

		vkFreeMemory(device, uniformBufferMemory, nullptr);
		vkFreeMemory(device, vertexBufferMemory, nullptr);
		vkFreeMemory(device, indexBufferMemory, nullptr);
	}
};

struct RoomDoors {
	bool north;
	bool east;
	bool south;
	bool west;
};

struct TextureIds {
	size_t wall;
	size_t floor;
	size_t ceiling;
};

class StructsHandler
{
	
};

