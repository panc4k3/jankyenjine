#pragma once
#define GLFW_INCLUDE_VULKAN
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <vector>
#include <chrono>
#include <atomic>

#include "VulkanHandler.h"
#include "DeviceHandler.h"
#include "QueueHandler.h"
#include "SurfaceHandler.h"
#include "SwapChainHandler.h"
#include "PipelineHandler.h"
#include "RenderPassHandler.h"
#include "BufferHandler.h"
#include "CommandPoolHandler.h"
#include "MemoryHandler.h"
#include "SyncHandler.h"
#include "DescriptorHandler.h"
#include "TextureHandler.h"
#include "StructsHandler.h"
#include "Light.h"
#include "Room.h"

enum Commands
{
	Quit,
	Continue,
	Help,
	DrawFps,
	TriangleCount,
	LoadLevel,
	LoadModel,
	NotFound,
	LoadEngine,
	UpdateEngine,
	StopEngine,
	InitEngine
};

class Engine
{
	public:
		bool framebufferResized = false;

		void Init(std::vector<Room>& rooms, std::vector<Model>& models);
		void Reset(std::vector<Room>& rooms, std::vector<Model>& models);
		void Start();
		void Update(std::atomic<int>& args);
		void Destroy();

		void setProperties(EngineProperties *engineProperties);
		int getCurrentFPS();
		size_t getTriangleCount();
		glm::vec4& getLightsEnabled();
		BufferHandler& getBufferHandler();

	private:
		GLFWwindow* window;
		VulkanHandler vulkanHandler;
		DeviceHandler deviceHandler;
		QueueHandler queueHandler;
		SurfaceHandler surfaceHandler;
		SwapChainHandler swapChainHandler;
		RenderPassHandler renderPassHandler;
		DescriptorHandler descriptorHandler;
		PipelineHandler pipelineHandler;
		BufferHandler bufferHandler;
		MemoryHandler memoryHandler;
		CommandPoolHandler commandPoolHandler;
		SyncHandler syncHandler;
		TextureHandler textureHandler;

		EngineProperties* m_engineProperties;
		VulkanAppProperties m_vulkanProperties;

		size_t currentFrame = 0;

		std::vector<Model> modelsObjects;

		glm::vec4 cameraPos = { 0.0f, 0.0f, 0.0f, 1.0f };

		glm::mat4 PivotPoint = glm::mat4(1.0f);
		glm::mat4 Camera = glm::mat4(1.0f);
		glm::mat4 View = glm::mat4(1.0f);

		float movementSpeed = 0.003f;

		glm::vec4 lightsEnabled = glm::vec4(1, 1, 1, 1);
		glm::vec4 lightsSkybox = glm::vec4(0, 0, 0, 0);

		double xpos, ypos;

		double frameEndTime = 0.0;
		double frameCurrentTime = 0.0;
		/*const std::vector<Vertex> vertices = {
			{{0.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f}},
			{{0.87f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}},
			{{0.87f, 0.5f, 0.0f}, {0.0f, 1.0f, 1.0f}},
			{{0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
			{{-0.87f, 0.5f, 0.0f}, {1.0f, 1.0f, 0.0f}},
			{{-0.87f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}},
			{{0.0f, -1.0f, 0.0f}, {1.0f, 0.0f, 1.0f}}
		};

		const std::vector<uint32_t> indices = {
			0, 1, 2,
			0, 2, 3,
			0, 3, 4,
			0, 4, 5,
			0, 5, 6,
			0, 6, 1
		};*/

		void initVulkan();
		void initWindow();

		void drawFrame();
		void createSwapChainAndDependencies();
		void updateSwapChain();
		void destroySwapChain();

		void updateUniformBuffer(uint32_t currentImage);
};

