#include "CommandPoolHandler.h"

VkResult CommandPoolHandler::createCommandPool(VkDevice& device, QueueFamilyIndices& indices)
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = indices.graphicsFamily.value();
	poolInfo.flags = 0; // Optional

	return vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool);
}

VkResult CommandPoolHandler::createBufferCommandPool(VkDevice& device, QueueFamilyIndices& indices)
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = indices.graphicsFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT; // Optional

	return vkCreateCommandPool(device, &poolInfo, nullptr, &bufferCommandPool);
}

VkCommandPool& CommandPoolHandler::getCommandPool()
{
	return commandPool;
}

VkCommandPool& CommandPoolHandler::getBufferCommandPool()
{
	return bufferCommandPool;
}

void CommandPoolHandler::Destroy(VkDevice& device)
{
	vkDestroyCommandPool(device, commandPool, nullptr);
	vkDestroyCommandPool(device, bufferCommandPool, nullptr);
}
