#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>

#include "Settings.h"

class SyncHandler
{
	public:
		VkResult createSemaphores(VkDevice& device);
		VkResult createFences(VkDevice& device);
		std::vector<VkSemaphore>& getImageAvaliableSemaphores();
		std::vector<VkSemaphore>& getRenderFinishedSemaphores();
		std::vector<VkFence>& getInFlightFences();
		void Destroy(VkDevice& device);

	private:
		std::vector<VkSemaphore> imageAvailableSemaphores;
		std::vector<VkSemaphore> renderFinishedSemaphores;
		std::vector<VkFence> inFlightFences;
};

