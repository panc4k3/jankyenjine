#include "VulkanHandler.h"
#include "Settings.h"

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData) {

	std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

	return VK_FALSE;
}

static VkDebugUtilsMessengerCreateInfoEXT getDebugUtilsMessengerCreateInfo() {
	VkDebugUtilsMessengerCreateInfoEXT createInfo = {};

	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = *MESSAGE_SEVERITY_FLAG_BITS;
	createInfo.messageType = *MESSAGE_TYPE_FLAG_BITS;
	createInfo.pfnUserCallback = debugCallback;
	createInfo.pUserData = nullptr;

	return createInfo;
}

void VulkanHandler::setProperties(VulkanAppProperties* vulkanAppProperties)
{
	m_vulkanAppProperties = vulkanAppProperties;
}

VkResult VulkanHandler::createInstance()
{
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = m_vulkanAppProperties->APPLICATION_NAME;
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = m_vulkanAppProperties->ENGINE_NAME;
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	auto extensions = getRequiredExtensions();

	createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	createInfo.ppEnabledExtensionNames = extensions.data();

	if (m_vulkanAppProperties->ENABLE_VALIDATION_LAYERS) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(m_vulkanAppProperties->VALIDATION_LAYERS.size());
		createInfo.ppEnabledLayerNames = m_vulkanAppProperties->VALIDATION_LAYERS.data();

		if (!checkValidationLayerSupport()) {
			throw std::runtime_error("Validations requested but not avaliable.");
		}

		createInfo.pNext = &getDebugUtilsMessengerCreateInfo();
	}
	else {
		createInfo.enabledLayerCount = 0;
	}

	return vkCreateInstance(&createInfo, nullptr, &instance);
}

VkInstance& VulkanHandler::getInstance()
{
	return instance;
}

VkResult VulkanHandler::setupDebugMessenger()
{
	auto function = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (function != nullptr) {
		return function(instance, &getDebugUtilsMessengerCreateInfo(), nullptr, &debugMessenger);
	}
	else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

void VulkanHandler::Destroy()
{
	if (m_vulkanAppProperties->ENABLE_VALIDATION_LAYERS) {
		DestroyDebugUtilsMessengerEXT();
	}

	vkDestroyInstance(instance, nullptr);
}

bool VulkanHandler::checkValidationLayerSupport() {
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availableLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	for (const char* layerName : m_vulkanAppProperties->VALIDATION_LAYERS) {
		bool layerFound = false;

		for (const auto& layerProperties : availableLayers) {
			if (strcmp(layerName, layerProperties.layerName) == 0) {
				layerFound = true;
				break;
			}
		}

		if (!layerFound) {
			return false;
		}
	}

	return true;
}

std::vector<const char*> VulkanHandler::getRequiredExtensions() {
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

	if (m_vulkanAppProperties->ENABLE_VALIDATION_LAYERS) {
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	return extensions;
}

void VulkanHandler::DestroyDebugUtilsMessengerEXT()
{
	auto function = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

	if (function != nullptr) {
		function(instance, debugMessenger, nullptr);
	}
}
