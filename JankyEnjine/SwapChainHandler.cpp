#include "SwapChainHandler.h"

SwapChainSupportDetails& SwapChainHandler::getSwapChainDetails()
{
	return details;
}

VkResult SwapChainHandler::createSwapChain(GLFWwindow* window, VkSurfaceKHR& surface, DeviceHandler& deviceHandler)
{
	std::cout << "test" << std::endl;
	deviceHandler.querySwapChainSupport(deviceHandler.getPhysicalDevice(), surface, details);
	deviceHandler.findQueueFamilies(deviceHandler.getPhysicalDevice(), surface);
	QueueFamilyIndices* indices = &deviceHandler.getIndices();

	VkSurfaceFormatKHR surfaceFormat = selectSwapSurfaceFormat(details.formats);
	VkPresentModeKHR presentMode = selectSwapPresentMode(details.presentModes);
	VkExtent2D extent = selectSwapExtent(details.capabilities, window);

	imageCount = details.capabilities.minImageCount + 1;
	if (details.capabilities.maxImageCount > 0 && imageCount > details.capabilities.maxImageCount) {
		imageCount = details.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	uint32_t queueFamilyIndices[] = { indices->graphicsFamily.value(), indices->presentFamily.value() };

	if (indices->graphicsFamily != indices->presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		//createInfo.queueFamilyIndexCount = 0; // Optional
		//createInfo.pQueueFamilyIndices = nullptr; // Optional
	}

	createInfo.preTransform = details.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	//createInfo.oldSwapchain = VK_NULL_HANDLE;

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;

	return vkCreateSwapchainKHR(deviceHandler.getDevice(), &createInfo, nullptr, &swapChain);
}

void SwapChainHandler::setSwapChainImages(VkDevice& device)
{
	vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());
}

VkSwapchainKHR& SwapChainHandler::getSwapChain()
{
	return swapChain;
}

std::vector<VkImage>& SwapChainHandler::getSwapChainImages()
{
	return swapChainImages;
}

std::vector<VkImageView>& SwapChainHandler::getSwapChainImageViews()
{
	return swapChainImageViews;
}

VkImageView& SwapChainHandler::getDepthImageView()
{
	return depthImageView;
}

VkExtent2D& SwapChainHandler::getSwapChainExtent()
{
	return swapChainExtent;
}

VkFormat& SwapChainHandler::getSwapChainImageFormat()
{
	return swapChainImageFormat;
}

VkResult SwapChainHandler::createImage(VkDevice& device, VkImage& image, VkFormat& format, VkImageUsageFlags usageFlags, int width, int height) {

	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = static_cast<uint32_t>(width);
	imageInfo.extent.height = static_cast<uint32_t>(height);
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usageFlags;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

	return vkCreateImage(device, &imageInfo, nullptr, &image);
}

VkResult SwapChainHandler::createImageViews(VkDevice& device)
{
	VkResult result = {};
	swapChainImageViews.resize(swapChainImages.size());

	for (size_t i = 0; i < swapChainImages.size(); i++) {
		result = createImageView(device, swapChainImages[i], swapChainImageFormat, swapChainImageViews[i], VK_IMAGE_ASPECT_COLOR_BIT);
		if (result != VK_SUCCESS) {
			break;
		}
	}

	return result;
}

VkResult SwapChainHandler::createImageView(VkDevice& device, VkImage image, VkFormat format, VkImageView& imageView, VkImageAspectFlags aspectFlags) {
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	return vkCreateImageView(device, &viewInfo, nullptr, &imageView);
}

VkResult SwapChainHandler::createDepthResources(VkPhysicalDevice& physicalDevice, VkDevice& device, DeviceHandler& deviceHandler, MemoryHandler memoryHandler)
{
	VkResult result = VK_SUCCESS;

	VkFormat depthFormat = deviceHandler.findDepthFormat();

	result = createImage(device, depthImage, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, swapChainExtent.width, swapChainExtent.height);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to create depth image. " << result << std::endl;
		return result;
	}

	result = memoryHandler.createDepthImageMemory(physicalDevice, device, depthImage);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to callocate depth image memory." << std::endl;
	}

	result = createImageView(device, depthImage, depthFormat, depthImageView, VK_IMAGE_ASPECT_DEPTH_BIT);
	if (result != VK_SUCCESS) {
		std::cout << "Unable to create depth image view" << std::endl;
		return result;
	}

	//transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

	return result;
}

void SwapChainHandler::Destroy(VkDevice& device)
{
	vkDestroyImageView(device, depthImageView, nullptr);
	vkDestroyImage(device, depthImage, nullptr);

	for (auto imageView : swapChainImageViews) {
		vkDestroyImageView(device, imageView, nullptr);
	}

	swapChainImageViews.clear();

	vkDestroySwapchainKHR(device, swapChain, nullptr);

	swapChain = VK_NULL_HANDLE;
}

VkSurfaceFormatKHR SwapChainHandler::selectSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}

	return availableFormats[0];
}

VkPresentModeKHR SwapChainHandler::selectSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
{
	for (const auto& availablePresentMode : availablePresentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentMode;
		}
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D SwapChainHandler::selectSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, GLFWwindow* window)
{
	std::cout << capabilities.currentExtent.width << std::endl;

	if (capabilities.currentExtent.width != UINT32_MAX) {
		return capabilities.currentExtent;
	}
	else {
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		VkExtent2D actualExtent = { 
			static_cast<uint32_t>(width), 
			static_cast<uint32_t>(height) 
		};

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}
