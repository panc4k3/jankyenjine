#pragma once
#include <glm/glm.hpp>

class VertexRoom
{
	public:
		VertexRoom(glm::vec3 position, glm::vec3 color, glm::vec2 textureCoord);
		glm::vec3 getPosition();
		glm::vec3 getColor();
		glm::vec2 getTexCoord();

		~VertexRoom();

	private:
		glm::vec3 pos;
		glm::vec3 color;
		glm::vec2 texCoord;
};

