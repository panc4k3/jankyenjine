#include "SurfaceHandler.h"

VkSurfaceKHR& SurfaceHandler::getSurface()
{
	return surface;
}

void SurfaceHandler::Destroy(VkInstance& instance)
{
	vkDestroySurfaceKHR(instance, surface, nullptr);
}
