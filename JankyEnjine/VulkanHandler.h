#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include "StructsHandler.h"

class VulkanHandler
{
	public:
		void setProperties(VulkanAppProperties* vulkanAppProperties);
		VkResult createInstance();
		VkInstance& getInstance();
		VkResult setupDebugMessenger();
		void Destroy();

	private:
		VkInstance instance;
		VkDebugUtilsMessengerEXT debugMessenger;
		VulkanAppProperties* m_vulkanAppProperties;

		bool checkValidationLayerSupport();
		std::vector<const char*> getRequiredExtensions();
		void DestroyDebugUtilsMessengerEXT();
};

