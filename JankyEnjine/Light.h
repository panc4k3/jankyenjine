#pragma once
#include <glm/glm.hpp>

class Light
{
public:
	Light();
	Light(glm::vec3 position, glm::vec4 color);

	void setPos(glm::vec3 pos);

	glm::vec3 getLightPos();
	glm::vec4 getLightColor();

private:

	glm::vec3 lightPos;
	glm::vec4 lightColor;
};

