#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 inLightPos;
layout(location = 3) in vec4 inLightColor;
layout(location = 4) in vec3 fragPos;
layout(location = 5) in vec3 normal;
layout(location = 6) in vec3 viewPos;
layout(location = 7) in vec4 lightsEnabled;
layout(location = 8) in float shinyness;

layout(location = 0) out vec4 outColor;

void main() {

	float ambientStrength= 0.5f;
	vec3 ambient = vec3(inLightColor) * ambientStrength;
	vec3 norm = normalize(normal);
	vec3 lightDir = normalize(inLightPos - fragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * vec3(inLightColor);
	
	float specularStrength = 0.5f;
	vec3 viewDir = normalize(viewPos - fragPos);
	vec3 reflectDir = reflect(-lightDir,  norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), shinyness);
	vec3 specular = specularStrength * spec * vec3(inLightColor);
	vec3 result;

	if(lightsEnabled.x == 0) {
		ambient = vec3(0, 0, 0);
	}

	if(lightsEnabled.y == 0) {
		diffuse = vec3(0, 0, 0);
	}

	if(lightsEnabled.z == 0) {
		specular = vec3(0, 0, 0);
	}

	result = (diffuse + specular + ambient) * texture(texSampler, fragTexCoord).rgb;

	if(lightsEnabled.w == 0) {
		result = texture(texSampler, fragTexCoord).rgb;
	}
    outColor = vec4(result, 1.0);
}