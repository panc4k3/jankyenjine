#include "QueueHandler.h"

void QueueHandler::setupQueues(DeviceHandler& deviceHandler)
{
	vkGetDeviceQueue(deviceHandler.getDevice(), deviceHandler.getIndices().graphicsFamily.value(), 0, &graphicsQueue);
	vkGetDeviceQueue(deviceHandler.getDevice(), deviceHandler.getIndices().presentFamily.value(), 0, &presentQueue);
}

VkQueue& QueueHandler::getGraphicsQueue()
{
	return graphicsQueue;
}

VkQueue& QueueHandler::getPresentQueue()
{
	return presentQueue;
}
