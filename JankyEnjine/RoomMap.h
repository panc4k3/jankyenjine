#pragma once
#include <map>
#include <vector>
#include <glm/glm.hpp>
#include "Room.h"

class RoomMap
{
	public:
		RoomMap(std::map<uint32_t, Room> rooms);
		~RoomMap();
		void addRoom(uint32_t roomId, Room room);
		Room getRoom(uint32_t roomId);

	private:
		std::map<uint32_t, Room> m_rooms;
};

