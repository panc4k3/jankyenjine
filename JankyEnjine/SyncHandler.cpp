#include "SyncHandler.h"

VkResult SyncHandler::createSemaphores(VkDevice& device)
{
	VkResult result;

	imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		result = vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]);
		if (result != VK_SUCCESS) {
			std::cout << "failed to create image semaphore for a frame!" << std::endl;
			return result;
		}

		result = vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]);
		if (result != VK_SUCCESS) {
			std::cout << "failed to create render semaphore for a frame!" << std::endl;
			return result;
		}
	}

	return result;
}

VkResult SyncHandler::createFences(VkDevice& device)
{
	VkResult result;
	inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		result = vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]);
		if (result != VK_SUCCESS) {
			std::cout << "failed to create in flight fence for a frame!" << std::endl;
			return result;
		}
	}

	return result;
}

std::vector<VkSemaphore>& SyncHandler::getImageAvaliableSemaphores()
{
	return imageAvailableSemaphores;
}

std::vector<VkSemaphore>& SyncHandler::getRenderFinishedSemaphores()
{
	return renderFinishedSemaphores;
}

std::vector<VkFence>& SyncHandler::getInFlightFences()
{
	return inFlightFences;
}

void SyncHandler::Destroy(VkDevice& device)
{
	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
		vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
		vkDestroyFence(device, inFlightFences[i], nullptr);
	}
}
