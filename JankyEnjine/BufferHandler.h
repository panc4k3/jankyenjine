#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>

#include "Settings.h"
#include "MemoryHandler.h"
#include "CommandPoolHandler.h"
#include "QueueHandler.h"
#include "Reader.h"

class BufferHandler
{
	public:
		VkResult createFramebuffers(VkDevice& device, VkExtent2D& swapChainExtent, std::vector<VkImageView>& swapChainImageViews, VkRenderPass& renderPass, VkImageView& depthImageView);
		VkResult createVertexBuffers(VkPhysicalDevice physicalDevice, VkDevice& device, MemoryHandler& memoryHandler, CommandPoolHandler& commandPoolHandler, QueueHandler& queueHandler, std::vector<Model>& models);
		VkResult createIndexBuffers(VkPhysicalDevice physicalDevice, VkDevice& device, MemoryHandler& memoryHandler, CommandPoolHandler& commandPoolHandler, QueueHandler& queueHandler, std::vector<Model>& models);
		VkResult createCommandBuffers(VkDevice& device, VkExtent2D& swapChainExtent, VkPipeline& graphicsPipeline, VkPipelineLayout& pipelineLayout, VkRenderPass& renderPass, VkCommandPool& commandPool, const std::vector<Model>& models);
		VkResult createUniformBuffers(VkPhysicalDevice physicalDevice, VkDevice device, MemoryHandler& memoryHandler, size_t swapChainImagesSize, std::vector<Model>& models);
		VkResult createTextureBuffer(VkPhysicalDevice physicalDevice, VkDevice& device, MemoryHandler memoryHandler, VkBuffer& stagingBuffer, VkDeviceMemory& stagingBufferMemory, VkDeviceSize& imageSize, stbi_uc* pixels);
		std::vector<VkCommandBuffer>& getCommandBuffers();
		void transitionImageLayout(VkDevice& device, VkCommandPool& commandPool, VkQueue& graphicsQueue, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
		void copyBufferToImage(VkDevice& device, VkCommandPool& commandPool, VkQueue& graphicsQueue, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
		void copyBuffer(VkDevice& device, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkCommandPool& commandPool, VkQueue& graphicsQueue);
		VkResult beginSingleTimeCommands(VkDevice& device, VkCommandPool& commandPool, VkCommandBuffer& commandBuffer);
		VkResult endSingleTimeCommands(VkDevice& device, VkQueue& graphicsQueue, VkCommandPool& commandPool, VkCommandBuffer& commandBuffer);
		void Destroy(VkDevice& device);
		void DestroyFramebuffers(VkDevice& device);
		void FreeCommandBuffers(VkDevice& device, VkCommandPool commandPool);

	private:
		std::vector<VkFramebuffer> swapChainFramebuffers;
		std::vector<VkCommandBuffer> commandBuffers;

		uint32_t findMemoryType(VkPhysicalDevice& physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);
		VkResult createBuffer(VkDevice& device, VkDeviceSize size, VkBufferUsageFlags usage, VkBuffer& buffer);
};
