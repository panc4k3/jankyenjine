#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>

#include "StructsHandler.h"

class DescriptorHandler
{
	public:
		VkResult createDescriptorSetLayouts(VkDevice& device);
		VkResult createDescriptorPool(VkDevice& device, uint32_t modelsSize);
		VkResult createDescriptorSets(VkDevice& device, uint32_t swapChainImagesSize, std::vector<Model>& models);
		std::vector<VkDescriptorSetLayout>& getDescriptorSetLayouts();
		void Destroy(VkDevice& device);
		void destroyDescriptorPool(VkDevice& device);

	private:
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
		VkDescriptorPool descriptorPool;
};

