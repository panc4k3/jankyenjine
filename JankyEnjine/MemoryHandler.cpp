#include "MemoryHandler.h"

void MemoryHandler::copyVertexBufferMemory(VkDevice& device, VkDeviceSize& bufferSize, VkDeviceMemory& bufferMemory, const std::vector<Vertex>& vertices)
{
	void* data;
	vkMapMemory(device, bufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(device, bufferMemory);
}

void MemoryHandler::copyIndiceBufferMemory(VkDevice& device, VkDeviceSize& bufferSize, VkDeviceMemory& bufferMemory, const std::vector<uint32_t>& indices)
{
	void* data;
	vkMapMemory(device, bufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices.data(), (size_t)bufferSize);
	vkUnmapMemory(device, bufferMemory);
}

void MemoryHandler::copyUniformBufferMemory(VkDevice& device, VkDeviceMemory& bufferMemory, UniformBufferObject& ubo)
{
	void* data;
	vkMapMemory(device, bufferMemory, 0, sizeof(ubo), 0, &data);
	memcpy(data, &ubo, sizeof(ubo));
	vkUnmapMemory(device, bufferMemory);
}

void MemoryHandler::copyTextureBufferMemory(VkDevice& device, VkDeviceSize bufferSize, VkDeviceMemory& bufferMemory, stbi_uc* pixels)
{
	void* data;
	vkMapMemory(device, bufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, pixels, static_cast<size_t>(bufferSize));
	vkUnmapMemory(device, bufferMemory);
}

VkResult MemoryHandler::createBufferMemory(VkPhysicalDevice& physicalDevice, VkDevice& device, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory)
{
	VkResult result;

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, properties);

	result = vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory);
	if (result != VK_SUCCESS){
		std::cout << "Failed to allocate buffer memory." << std::endl;
		return result;
	}

	vkBindBufferMemory(device, buffer, bufferMemory, 0);

	return result;
}

VkResult MemoryHandler::createTextureMemory(VkPhysicalDevice& physicalDevice, VkDevice& device, VkImage& textureImage, size_t index)
{
	VkResult result = VK_SUCCESS;

	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(device, textureImage, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	result = vkAllocateMemory(device, &allocInfo, nullptr, &textureImageMemory[index]);
	if (result != VK_SUCCESS) {
		std::cout << "Failed to allocate image memory." << std::endl;
		return result;
	}

	vkBindImageMemory(device, textureImage, textureImageMemory[index], 0);

	return result;
}

VkResult MemoryHandler::createDepthImageMemory(VkPhysicalDevice& physicalDevice, VkDevice& device, VkImage& depthImage)
{
	VkResult result;

	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(device, depthImage, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	result = vkAllocateMemory(device, &allocInfo, nullptr, &depthImageMemory);
	if (result != VK_SUCCESS) {
		std::cout << "Failed to allocate image memory." << std::endl;
		return result;
	}

	vkBindImageMemory(device, depthImage, depthImageMemory, 0);

	return result;
}

void MemoryHandler::freeTextureImageMemory(VkDevice& device) {
	for (VkDeviceMemory& textureImageMem : textureImageMemory) {
		vkFreeMemory(device, textureImageMem, nullptr);
	}
}

void MemoryHandler::freeDepthImageMemory(VkDevice& device)
{
	vkFreeMemory(device, depthImageMemory, nullptr);
}

void MemoryHandler::FreeMemory(VkDevice& device)
{
	freeTextureImageMemory(device);
	freeDepthImageMemory(device);
}

uint32_t MemoryHandler::findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}

	throw std::runtime_error("failed to find suitable memory type!");
}
