#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <map>

#include "StructsHandler.h"
#include "Settings.h"
#include "Light.h"

class Room
{
	public:
		Room(RoomDoors doors, glm::vec3 _startPoint, float _length, float _height, float _width, TextureIds textureIds, uint32_t indexStart, Light light);

		struct Part {
			size_t textureId;
			std::vector<Vertex> vertices;
			std::vector<uint32_t> indices;

			std::vector<Vertex> getVertices() {
				return vertices;
			}

			std::vector<uint32_t> getIndices() {
				return indices;
			}

			size_t getTextureId() {
				return textureId;
			}
		};

		std::array<Part, 3> parts;

		float getLength();
		Light getLight();
		glm::vec3 getCentre();
		glm::vec3 getStart();
		uint32_t getIndex();
		void setIndex(uint32_t index);

		~Room();
		
	private:
		glm::vec3 m_startPoint;
		glm::vec3 m_centre;

		float m_lengthX;
		float m_heightZ;
		float m_depthY;

		glm::vec3 defaultColor;
		glm::vec2 defaultTex;

		Light m_light;

		std::map<char, glm::vec3> roomVertices;

		uint32_t index;

		void setVertices();
		void setCentre();
		void drawWall(bool door, float doorWidth, glm::vec2 textureStartPoint, float textureTileSize, float textureTileHeight, std::map<char, glm::vec3> wallVertices);
		void drawFloor(glm::vec2 textureStartPoint, float textureTileSizeX, float textureTileSizeY);
		void drawCeiling(glm::vec2 textureStartPoint, float textureTileSizeX, float textureTileSizeY);
		glm::vec3 calculateNormal(std::array<glm::vec3, 3> triangle);
};

