#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <glm/glm.hpp>
#include <map>

const int MAX_FRAMES_IN_FLIGHT = 2;

const VkDynamicState DYNAMIC_STATES[] = {
	VK_DYNAMIC_STATE_VIEWPORT,
	VK_DYNAMIC_STATE_LINE_WIDTH
};

const int DYNAMIC_STATES_SIZE = 2;

const VkDebugUtilsMessageSeverityFlagsEXT MESSAGE_SEVERITY_FLAG_BITS[] = {
	//VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
	VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
	VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
	VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
};

const VkDebugUtilsMessageTypeFlagsEXT MESSAGE_TYPE_FLAG_BITS[] = {
	VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
	VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
	VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
};

const VkClearColorValue CLEAR_COLOR = { 
	0.0f, 
	0.0f, 
	0.0f, 
	1.0f
};

const VkClearDepthStencilValue CLEAR_DEPTH_STENCIL = {
	1.0f,
	0
};

const std::map<std::string, size_t> TEXTURE_IDS = {
	{"skybox", 0},
	{"wall", 1},
	{"floor", 2},
	{"ceiling", 3}
};

static glm::vec3 GetTextureCoordsAndSize(uint32_t id) {
	switch (id) {
	case 0:
		return glm::vec3(0.0f, 0.0f, 0.25f);

	case 1:
		return glm::vec3(0.0f, 0.25f, 0.25f);

	case 2:
		return glm::vec3(0.0f, 0.50f, 0.25f);

	case 3:
		return glm::vec3(0.0f, 0.75f, 0.25f);

	case 4:
		return glm::vec3(0.25f, 0.0f, 0.25f);

	case 5:
		return glm::vec3(0.25f, 0.25f, 0.25f);

	case 6:
		return glm::vec3(0.25f, 0.50f, 0.25f);

	case 7:
		return glm::vec3(0.25f, 0.75f, 0.25f);

	case 8:
		return glm::vec3(0.50f, 0.0f, 0.25f);

	case 9:
		return glm::vec3(0.50f, 0.25f, 0.25f);

	case 10:
		return glm::vec3(0.50f, 0.50f, 0.25f);

	case 11:
		return glm::vec3(0.50f, 0.75f, 0.25f);

	case 12:
		return glm::vec3(0.75f, 0.0f, 0.25f);

	case 13:
		return glm::vec3(0.75f, 0.25f, 0.25f);

	case 14:
		return glm::vec3(0.75f, 0.50f, 0.25f);

	case 15:
		return glm::vec3(0.75f, 0.75f, 0.25f);

	default:
		std::cout << "Unable to get texture." << std::endl;
		return glm::vec3();
	}
}

class Settings
{
};

