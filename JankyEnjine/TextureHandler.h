#pragma once

#include "Reader.h"
#include "BufferHandler.h"
#include "SwapChainHandler.h"

class TextureHandler
{
	public:
		VkResult createTextureImagesAndViews(VkPhysicalDevice physicalDevice, VkDevice device, BufferHandler& bufferHandler, MemoryHandler& memoryHandler, CommandPoolHandler& commandPoolHandler, QueueHandler& queueHandler, std::vector<Model>& models);
		VkResult createTextureSamplers(VkDevice& device, std::vector<Model>& models);
		void Destroy(VkDevice& device);

		std::vector<VkImage> textureImages;
		std::vector<VkImageView> textureImageViews;
		std::vector<VkSampler> textureImageSamplers;
};

