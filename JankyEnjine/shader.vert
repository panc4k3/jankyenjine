#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
	vec3 cameraPos;
	vec3 lightPos;
	vec4 lightColor;
	vec4 lightsEnabled;
	float specular;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inNormal;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexCoord;
layout(location = 2) out vec3 outLightPos;
layout(location = 3) out vec4 outLightColor;
layout(location = 4) out vec3 fragPos;
layout(location = 5) out vec3 normal;
layout(location = 6) out vec3 viewPos;
layout(location = 7) out vec4 lightsEnabled;
layout(location = 8) out float shinyness;

out gl_PerVertex {
	vec4 gl_Position;
};

void main() {
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition.xyz, 1.0);
	outLightPos = ubo.lightPos;
	outLightColor = ubo.lightColor;
    fragColor = inColor;
	fragTexCoord = inTexCoord;
	fragPos = vec3(ubo.model * vec4(inPosition, 1.0));
	normal = inNormal;
	viewPos = ubo.cameraPos;
	lightsEnabled = ubo.lightsEnabled;
	shinyness = ubo.specular;
}