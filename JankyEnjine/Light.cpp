#include "Light.h"

Light::Light()
{
}

Light::Light(glm::vec3 position, glm::vec4 color)
{
	lightPos = position;
	lightColor = color;
}

void Light::setPos(glm::vec3 pos)
{
	lightPos = pos;
}

glm::vec3 Light::getLightPos()
{
	return lightPos;
}

glm::vec4 Light::getLightColor()
{
	return lightColor;
}
