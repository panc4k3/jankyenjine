#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <array>

#include "DeviceHandler.h"
#include "MemoryHandler.h"
#include "StructsHandler.h"

class SwapChainHandler
{
	public:
		SwapChainSupportDetails& getSwapChainDetails();
		VkResult createSwapChain(GLFWwindow* window, VkSurfaceKHR& surface, DeviceHandler& deviceHandler);
		void setSwapChainImages(VkDevice& device);
		VkSwapchainKHR& getSwapChain();
		std::vector<VkImage>& getSwapChainImages();
		std::vector<VkImageView>& getSwapChainImageViews();
		VkImageView& getDepthImageView();
		VkExtent2D& getSwapChainExtent();
		VkFormat& getSwapChainImageFormat();
		VkResult createImage(VkDevice& device, VkImage& image, VkFormat& format, VkImageUsageFlags usageFlags, int width, int height);
		VkResult createImageViews(VkDevice& device);
		VkResult createImageView(VkDevice& device, VkImage image, VkFormat format, VkImageView& imageView, VkImageAspectFlags aspectFlags);
		VkResult createDepthResources(VkPhysicalDevice& physicalDevice, VkDevice& device, DeviceHandler& deviceHandler, MemoryHandler memoryHandler);
		void Destroy(VkDevice& device);

	private:
		VkSwapchainKHR swapChain;

		std::vector<VkImage> swapChainImages;
		std::vector<VkImageView> swapChainImageViews;

		VkImage depthImage;
		VkImageView depthImageView;

		VkFormat swapChainImageFormat;
		VkExtent2D swapChainExtent;

		SwapChainSupportDetails details;
		uint32_t imageCount;

		VkSurfaceFormatKHR selectSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
		VkPresentModeKHR selectSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
		VkExtent2D selectSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, GLFWwindow* window);
};

