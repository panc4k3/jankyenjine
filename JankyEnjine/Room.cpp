#include "Room.h"

Room::Room(RoomDoors doors, glm::vec3 _startPoint, float _length, float _height, float _width, TextureIds textureIds, uint32_t indexStart, Light light) {
	m_startPoint = _startPoint;
	m_lengthX = _length;
	m_heightZ = _height;
	m_depthY = _width;

	parts[0].textureId = textureIds.wall;
	parts[1].textureId = textureIds.floor;
	parts[2].textureId = textureIds.ceiling;
	
	m_light = light;

	//Grey
	defaultColor =* new glm::vec3(0.5f, 0.5f, 0.5f);
	defaultTex =* new glm::vec2(0, 0);
	index = indexStart;
	roomVertices = {};
	setVertices();

	std::map<char, glm::vec3> wallVertices;
	wallVertices.insert(std::make_pair('a', roomVertices['a']));
	wallVertices.insert(std::make_pair('b', roomVertices['b']));
	wallVertices.insert(std::make_pair('c', roomVertices['e']));
	wallVertices.insert(std::make_pair('d', roomVertices['f']));
	drawWall(doors.south, 1.0f, defaultTex, m_lengthX, m_heightZ, wallVertices);
	
	wallVertices.clear();
	wallVertices.insert(std::make_pair('a', roomVertices['b']));
	wallVertices.insert(std::make_pair('b', roomVertices['c']));
	wallVertices.insert(std::make_pair('c', roomVertices['f']));
	wallVertices.insert(std::make_pair('d', roomVertices['g']));
	drawWall(doors.east, 1.0f, defaultTex, m_depthY, m_heightZ, wallVertices);

	wallVertices.clear();
	wallVertices.insert(std::make_pair('a', roomVertices['c']));
	wallVertices.insert(std::make_pair('b', roomVertices['d']));
	wallVertices.insert(std::make_pair('c', roomVertices['g']));
	wallVertices.insert(std::make_pair('d', roomVertices['h']));
	drawWall(doors.north, 1.0f, defaultTex, m_lengthX, m_heightZ, wallVertices);

	wallVertices.clear();
	wallVertices.insert(std::make_pair('a', roomVertices['a']));
	wallVertices.insert(std::make_pair('b', roomVertices['d']));
	wallVertices.insert(std::make_pair('c', roomVertices['e']));
	wallVertices.insert(std::make_pair('d', roomVertices['h']));
	drawWall(doors.west, 1.0f, defaultTex, m_depthY, m_heightZ, wallVertices);

	drawFloor(defaultTex, m_lengthX, m_depthY);

	drawCeiling(defaultTex, m_lengthX, m_depthY);

	setCentre();
}

float Room::getLength() {
	return m_lengthX;
}

Light Room::getLight()
{
	return m_light;
}

glm::vec3 Room::getCentre() {
	return m_centre;
}

glm::vec3 Room::getStart() {
	return m_startPoint;
}

uint32_t Room::getIndex()
{
	return index;
}

void Room::setIndex(uint32_t index)
{
	this->index = index;
}

Room::~Room()
{

}

void Room::setVertices()
{
	//     a____________________b
	//    /|                   /|
	//  d/_|_________________c/ |
	//  | e|_________________|__|f
	//  | /                  | /
	// h|/___________________|/g

	//Start Vertex(a)	0
	roomVertices.insert(std::make_pair('e', m_startPoint));
	//Vertex (f)		1
	roomVertices.insert(std::make_pair('f', m_startPoint + glm::vec3(m_lengthX, 0, 0)));
	//Vertex (h)		2
	roomVertices.insert(std::make_pair('h', m_startPoint + glm::vec3(0, m_depthY, 0)));
	//Vertex (g)		3
	roomVertices.insert(std::make_pair('g', m_startPoint + glm::vec3(m_lengthX, m_depthY, 0)));
	//Vertex (a)		4
	roomVertices.insert(std::make_pair('a', m_startPoint + glm::vec3(0, 0, m_heightZ)));
	//Vertex (b)		5
	roomVertices.insert(std::make_pair('b', m_startPoint + glm::vec3(m_lengthX, 0, m_heightZ)));
	//Vertex (d)		6
	roomVertices.insert(std::make_pair('d', m_startPoint + glm::vec3(0, m_depthY, m_heightZ)));
	//Vertex (c)		7
	roomVertices.insert(std::make_pair('c', m_startPoint + glm::vec3(m_lengthX, m_depthY, m_heightZ)));
}

void Room::drawWall(bool door, float doorWidth, glm::vec2 textureStartPoint, float textureTileSize, float textureTileHeight, std::map<char, glm::vec3> wallVertices) {

	/* 
	   a_________e___f_________ b
		|        |   |         |
		|		 |	 |		   |
		|		 |	 |		   |
	   c|________|___|_________|d
                 g   h
	*/

	if (door) {
		//Vertex (a)
		parts[0].vertices.push_back({ wallVertices['a'], defaultColor, textureStartPoint, { 0, 0, 0 } });
		if (index != 0) {
			index++;
		}
		uint32_t a = index;
		//Vertex (b)
		parts[0].vertices.push_back({ wallVertices['b'], defaultColor, textureStartPoint + glm::vec2(textureTileSize / 2, 0), { 0, 0, 0 } });
		index++;
		uint32_t b = index;
		//Vertex (c)
		parts[0].vertices.push_back({ wallVertices['c'] , defaultColor, textureStartPoint + glm::vec2(0, textureTileHeight), { 0, 0, 0 } });
		index++;
		uint32_t c = index;
		//Vertex (d)
		parts[0].vertices.push_back({ wallVertices['d'], defaultColor, textureStartPoint + glm::vec2(textureTileSize / 2, textureTileHeight), { 0, 0, 0 } });
		index++;
		uint32_t d = index;

		glm::vec3 startPoint = wallVertices['a'];

		uint32_t e, f, g, h;

		if (startPoint.y < wallVertices['b'].y) {
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y + (m_depthY / 2.0) - (doorWidth / 2.0), startPoint.z), defaultColor, textureStartPoint + glm::vec2((m_depthY / 2.0) - (doorWidth / 2.0), 0), { 0, 0, 0 } });
			index++;
			e = index;
			//Vertex (f)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y + (m_depthY / 2.0) + (doorWidth / 2.0), startPoint.z), defaultColor, textureStartPoint, { 0, 0, 0 } });
			index++;
			f = index;
			//Vertex (g)	
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y + (m_depthY / 2.0) - (doorWidth / 2.0), startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2((m_depthY / 2.0) - (doorWidth / 2.0), textureTileHeight), { 0, 0, 0 } });
			index++;
			g = index;
			//Vertex (h)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y + (m_depthY / 2.0) + (doorWidth / 2.0), startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2(0, textureTileHeight), { 0, 0, 0 } });
			index++;
			h = index;
		}
		else if (startPoint.y > wallVertices['b'].y) {
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y - (m_depthY / 2.0) + (doorWidth / 2.0), startPoint.z), defaultColor, textureStartPoint + glm::vec2((m_depthY / 2.0) - (doorWidth / 2.0), 0), { 0, 0, 0 } });
			index++;
			e = index;
			//Vertex (f)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y - (m_depthY / 2.0) - (doorWidth / 2.0), startPoint.z), defaultColor, textureStartPoint, { 0, 0, 0 } });
			index++;
			f = index;
			//Vertex (g)	
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y - (m_depthY / 2.0) + (doorWidth / 2.0), startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2((m_depthY / 2.0) - (doorWidth / 2.0), textureTileHeight), { 0, 0, 0 } });
			index++;
			g = index;
			//Vertex (h)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x, startPoint.y - (m_depthY / 2.0) - (doorWidth / 2.0), startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2(0, textureTileHeight), { 0, 0, 0 } });
			index++;
			h = index;
		}
		else if(startPoint.x < wallVertices['b'].x) {
			//Vertex (e)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x + (m_lengthX / 2.0) - (doorWidth / 2.0), startPoint.y, startPoint.z), defaultColor, textureStartPoint + glm::vec2((m_lengthX / 2.0) + (doorWidth / 2.0), 0), { 0, 0, 0 } });
			index++;
			e = index;
			//Vertex (f)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x + (m_lengthX / 2.0) + (doorWidth / 2.0), startPoint.y, startPoint.z), defaultColor, textureStartPoint, { 0, 0, 0 } });
			index++;
			f = index;
			//Vertex (g)	
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x + (m_lengthX / 2.0) - (doorWidth / 2.0), startPoint.y, startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2((m_lengthX / 2.0) + (doorWidth / 2.0), textureTileHeight), { 0, 0, 0 } });
			index++;
			g = index;
			//Vertex (h)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x + (m_lengthX / 2.0) + (doorWidth / 2.0), startPoint.y, startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2(0, textureTileHeight), { 0, 0, 0 } });
			index++;
			h = index;
		}
		else {
			//Vertex (e)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x - (m_lengthX / 2.0) + (doorWidth / 2.0), startPoint.y, startPoint.z), defaultColor, textureStartPoint + glm::vec2((m_lengthX / 2.0) - (doorWidth / 2.0), 0), { 0, 0, 0 } });
			index++;
			e = index;
			//Vertex (f)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x - (m_lengthX / 2.0) - (doorWidth / 2.0), startPoint.y, startPoint.z), defaultColor, textureStartPoint, { 0, 0, 0 } });
			index++;
			f = index;
			//Vertex (g)	
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x - (m_lengthX / 2.0) + (doorWidth / 2.0), startPoint.y, startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2((m_lengthX / 2.0) - (doorWidth / 2.0), textureTileHeight), { 0, 0, 0 } });
			index++;
			g = index;
			//Vertex (h)
			parts[0].vertices.push_back({ *new glm::vec3(startPoint.x - (m_lengthX / 2.0) - (doorWidth / 2.0), startPoint.y, startPoint.z - m_heightZ), defaultColor, textureStartPoint + glm::vec2(0, textureTileHeight), { 0, 0, 0 } });
			index++;
			h = index;
		}

		//Triangle (a, g, e)
		parts[0].indices.push_back(a);
		parts[0].indices.push_back(e);
		parts[0].indices.push_back(g);

		//Triangle (a, c, g)
		parts[0].indices.push_back(a);
		parts[0].indices.push_back(c);
		parts[0].indices.push_back(g);

		//Triangle (f, d, b)
		parts[0].indices.push_back(f);
		parts[0].indices.push_back(d);
		parts[0].indices.push_back(b);

		//Triangle (f, h, d)
		parts[0].indices.push_back(f);
		parts[0].indices.push_back(h);
		parts[0].indices.push_back(d);

		glm::vec3 tri_normal = calculateNormal({ parts[0].vertices[a].pos, parts[0].vertices[e].pos, parts[0].vertices[g].pos });
		parts[0].vertices[a].normal = tri_normal;
		parts[0].vertices[b].normal = tri_normal;
		parts[0].vertices[c].normal = tri_normal;
		parts[0].vertices[d].normal = tri_normal;
		parts[0].vertices[e].normal = tri_normal;
		parts[0].vertices[f].normal = tri_normal;
		parts[0].vertices[g].normal = tri_normal;
		parts[0].vertices[h].normal = tri_normal;
	}

	else {
		//Vertex (a)
		parts[0].vertices.push_back({ wallVertices['a'], defaultColor, textureStartPoint, { 0, 0, 0 } });
		if (index != 0) {
			index++;
		}
		uint32_t a = index;
		//Vertex (b)
		parts[0].vertices.push_back({ wallVertices['b'], defaultColor, textureStartPoint + glm::vec2(textureTileSize, 0), { 0, 0, 0 } });
		index++;
		uint32_t b = index;
		//Vertex (c)
		parts[0].vertices.push_back({ wallVertices['c'] , defaultColor, textureStartPoint + glm::vec2(0, textureTileHeight), { 0, 0, 0 } });
		index++;
		uint32_t c = index;
		//Vertex (d)
		parts[0].vertices.push_back({ wallVertices['d'], defaultColor, textureStartPoint + glm::vec2(textureTileSize, textureTileHeight), { 0, 0, 0 } });
		index++;
		uint32_t d = index;

		//Triangle (b, d, c)
		parts[0].indices.push_back(b);
		parts[0].indices.push_back(d);
		parts[0].indices.push_back(c);

		//Triangle (c, a, b)
		parts[0].indices.push_back(c);
		parts[0].indices.push_back(a);
		parts[0].indices.push_back(b);

		glm::vec3 tri_normal = calculateNormal({ parts[0].vertices[b].pos, parts[0].vertices[c].pos, parts[0].vertices[d].pos });
		parts[0].vertices[a].normal = tri_normal;
		parts[0].vertices[b].normal = tri_normal;
		parts[0].vertices[c].normal = tri_normal;
		parts[0].vertices[d].normal = tri_normal;
	}
}

void Room::drawFloor(glm::vec2 textureStartPoint, float textureTileSizeX, float textureTileSizeY)
{
	index = 0;
	/*a___________b
	   |         |
	   |         |
	   |         |
	  c|_________|d
	*/
	//Vertex (a)
	parts[1].vertices.push_back({ m_startPoint + glm::vec3(0,0,m_heightZ), defaultColor, textureStartPoint });
	uint32_t a = index;
	//Vertex (b)
	parts[1].vertices.push_back({ *new glm::vec3(m_startPoint.x + m_lengthX, m_startPoint.y, m_startPoint.z + m_heightZ), defaultColor, textureStartPoint + glm::vec2(textureTileSizeX, 0), { 0, 0, 0 } });
	index++;
	uint32_t b = index;
	//Vertex (c)
	parts[1].vertices.push_back({ *new glm::vec3(m_startPoint.x, m_startPoint.y + m_depthY, m_startPoint.z + m_heightZ), defaultColor, textureStartPoint + glm::vec2(0, textureTileSizeY), { 0, 0, 0 } });
	index++;
	uint32_t c = index;
	//Vertex (d)
	parts[1].vertices.push_back({ *new glm::vec3(m_startPoint.x + m_lengthX, m_startPoint.y + m_depthY, m_startPoint.z + m_heightZ), defaultColor, textureStartPoint + glm::vec2(textureTileSizeX, textureTileSizeY), { 0, 0, 0 } });
	index++;
	uint32_t d = index;

	//Triangle (a, c, d)
	parts[1].indices.push_back(a);
	parts[1].indices.push_back(c);
	parts[1].indices.push_back(d);

	//Triangle (a, d ,b)
	parts[1].indices.push_back(a);
	parts[1].indices.push_back(d);
	parts[1].indices.push_back(b);

	glm::vec3 tri_normal = calculateNormal({ parts[1].vertices[a].pos, parts[1].vertices[c].pos, parts[1].vertices[d].pos });
	parts[1].vertices[a].normal = tri_normal;
	parts[1].vertices[b].normal = tri_normal;
	parts[1].vertices[c].normal = tri_normal;
	parts[1].vertices[d].normal = tri_normal;
}

void Room::drawCeiling(glm::vec2 textureStartPoint, float textureTileSizeX, float textureTileSizeY)
{
	index = 0;
	/*a___________b
	   |         |
	   |         |
	   |         |
	  c|_________|d
	*/
	//Vertex (a)
	parts[2].vertices.push_back({ m_startPoint, defaultColor, textureStartPoint, { 0, 0, 0 } });
	uint32_t a = index;
	//Vertex (b)
	parts[2].vertices.push_back({ *new glm::vec3(m_startPoint.x + m_lengthX, m_startPoint.y, m_startPoint.z), defaultColor, textureStartPoint + glm::vec2(textureTileSizeX, 0), { 0, 0, 0 } });
	index++;
	uint32_t b = index;
	//Vertex (c)
	parts[2].vertices.push_back({ *new glm::vec3(m_startPoint.x, m_startPoint.y + m_depthY, m_startPoint.z), defaultColor, textureStartPoint + glm::vec2(0, textureTileSizeY), { 0, 0, 0 } });
	index++;
	uint32_t c = index;
	//Vertex (d)
	parts[2].vertices.push_back({ *new glm::vec3(m_startPoint.x + m_lengthX, m_startPoint.y + m_depthY, m_startPoint.z), defaultColor, textureStartPoint + glm::vec2(textureTileSizeX, textureTileSizeY), { 0, 0, 0 } });
	index++;
	uint32_t d = index;

	//Triangle (a, c, d)
	parts[2].indices.push_back(a);
	parts[2].indices.push_back(c);
	parts[2].indices.push_back(d);

	//Triangle (a, d ,b)
	parts[2].indices.push_back(a);
	parts[2].indices.push_back(d);
	parts[2].indices.push_back(b);

	glm::vec3 tri_normal = calculateNormal({ parts[2].vertices[a].pos, parts[2].vertices[d].pos, parts[2].vertices[c].pos });
	parts[2].vertices[a].normal = tri_normal;
	parts[2].vertices[b].normal = tri_normal;
	parts[2].vertices[c].normal = tri_normal;
	parts[2].vertices[d].normal = tri_normal;
}

void Room::setCentre()
{
	m_centre = *new glm::vec3(m_startPoint.x + (m_lengthX / 2.0), m_startPoint.y + (m_depthY / 2.0), m_startPoint.z + (m_heightZ / 2.0));
}

glm::vec3 Room::calculateNormal(std::array<glm::vec3, 3> triangle) {
	glm::vec3 u = triangle[1] - triangle[0];
	glm::vec3 v = triangle[2] - triangle[0];

	return {
		(u.y * v.z) - (u.z * v.y),
		(u.z * v.x) - (u.x * v.z),
		(u.x * v.y) - (u.y * v.x)
	};
}