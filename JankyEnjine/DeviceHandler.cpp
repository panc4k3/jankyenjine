#include "DeviceHandler.h"

void DeviceHandler::setProperties(VulkanAppProperties* vulkanAppProperties)
{
	m_vulkanAppProperties = vulkanAppProperties;
}

VkResult DeviceHandler::selectPhysicalDevice(VulkanHandler* vkHandler, VkSurfaceKHR surface, SwapChainSupportDetails& swapChainDetails)
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(vkHandler->getInstance(), &deviceCount, nullptr);

	if (deviceCount == 0) {
		std::cout << "failed to find GPUs with Vulkan support!" << std::endl;
		return VK_ERROR_INITIALIZATION_FAILED;
	}

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(vkHandler->getInstance(), &deviceCount, devices.data());

	std::multimap<int, VkPhysicalDevice> device_options;

	for (const auto& device : devices) {
		device_options.insert(std::make_pair(rateDeviceScore(device, surface, swapChainDetails), device));
	}

	if (device_options.rbegin()->first > 0) {
		physicalDevice = device_options.rbegin()->second;
		return VK_SUCCESS;
	}
	else {	
		std::cout << "Failed to find a suitable GPU." << std::endl;
		return VK_ERROR_FEATURE_NOT_PRESENT;
	}

	return VK_ERROR_EXTENSION_NOT_PRESENT;
}

VkResult DeviceHandler::createLogicalDevice() {
	float queuePriority = 1.0f;

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos = {};
	std::set<uint32_t> uniqueQueueFamilies = { 
		indices.graphicsFamily.value(), 
		indices.presentFamily.value() 
	};

	for (uint32_t queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE;

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(m_vulkanAppProperties->DEVICE_EXTENSIONS.size());
	createInfo.ppEnabledExtensionNames = m_vulkanAppProperties->DEVICE_EXTENSIONS.data();

	if (m_vulkanAppProperties->ENABLE_VALIDATION_LAYERS) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(m_vulkanAppProperties->VALIDATION_LAYERS.size());
		createInfo.ppEnabledLayerNames = m_vulkanAppProperties->VALIDATION_LAYERS.data();
	}
	else {
		createInfo.enabledLayerCount = 0;
	}

	return vkCreateDevice(physicalDevice, &createInfo, nullptr, &device);
}

VkDevice& DeviceHandler::getDevice()
{
	return device;
}

VkPhysicalDevice& DeviceHandler::getPhysicalDevice()
{
	return physicalDevice;
}

void DeviceHandler::findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface)
{
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto& queueFamily : queueFamilies) {
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

		if (queueFamily.queueCount > 0) {
			if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i;
			}

			if (presentSupport) {
				indices.presentFamily = i;
			}
		}

		if (indices.isComplete()) {
			break;
		}
		i++;
	}
}

QueueFamilyIndices& DeviceHandler::getIndices()
{
	return indices;
}

VkFormat DeviceHandler::findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
	for (VkFormat format : candidates) {
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
		
		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		}
		else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}

	throw std::runtime_error("Failed to find supported format.");
}

VkFormat DeviceHandler::findDepthFormat()
{
	return findSupportedFormat(
		{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
		VK_IMAGE_TILING_OPTIMAL,
		VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
	);
}

bool DeviceHandler::hasStencilComponent(VkFormat format) {
	return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

void DeviceHandler::Destroy()
{
	vkDestroyDevice(device, nullptr);
}

int DeviceHandler::rateDeviceScore(VkPhysicalDevice device, VkSurfaceKHR surface, SwapChainSupportDetails& swapChainDetails) {
	VkPhysicalDeviceProperties deviceProperties;
	VkPhysicalDeviceFeatures deviceFeatures;

	vkGetPhysicalDeviceProperties(device, &deviceProperties);
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

	querySwapChainSupport(device, surface, swapChainDetails);

	int score = 0;

	if (!deviceFeatures.geometryShader) {
		std::cout << "Required geometry shaders not supported by GPU." << std::endl;
		return score;
	}

	if (!deviceFeatures.samplerAnisotropy) {
		std::cout << "Required anisotropy sampler not supported by GPU." << std::endl;
		return score;
	}

	findQueueFamilies(device, surface);
	
	if (!indices.isComplete()) {
		std::cout << "Required graphics queues not supported by GPU." << std::endl;
		return score;
	}

	if (!checkDeviceExtensionSupport(device)) {
		std::cout << "Required extensions not supported by GPU." << std::endl;
		return score;
	}

	if (swapChainDetails.formats.empty() || swapChainDetails.presentModes.empty()) {
		std::cout << "Required swapchain formats or presentation modes not supported by GPU." << std::endl;
		return score;
	}

	if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
		score += 500;
	}

	score += deviceProperties.limits.maxImageDimension2D;
	score += deviceProperties.limits.maxComputeSharedMemorySize;

	return score;
}

void DeviceHandler::querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface, SwapChainSupportDetails& swapChainDetails) {

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &swapChainDetails.capabilities);

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

	if (formatCount != 0) {
		swapChainDetails.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, swapChainDetails.formats.data());
	}

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		swapChainDetails.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, swapChainDetails.presentModes.data());
	}
}

bool DeviceHandler::checkDeviceExtensionSupport(VkPhysicalDevice device) {
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

	std::set<std::string> requiredExtensions(m_vulkanAppProperties->DEVICE_EXTENSIONS.begin(), m_vulkanAppProperties->DEVICE_EXTENSIONS.end());

	for (const auto& extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();
}
