#pragma once
#include <fstream>
#include "RoomMap.h"
#include <nlohmann/json.hpp>
#include <iostream>
#include <iomanip>

class Writer
{
	public:
		Writer();
		~Writer();
		void writeLevel(RoomMap level);

	private:
		const std::string path_to_level;
		const std::string file_name = "level.json";

};

