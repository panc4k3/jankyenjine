# JankyEnjine

A basic Vulkan API implementation project with "Doom" style rooms and textures.

![01](/uploads/260f43fc359eae4584f04ea5c5998624/01.PNG)

![02](/uploads/00487d3006ad0d1f0b1cbdc16aea0068/02.PNG)

![03](/uploads/d1a68b4bb502a05d9e115b493fa28fe0/03.PNG)

![04](/uploads/f580d10e5d477c293cb126e24b430ac8/04.PNG)
